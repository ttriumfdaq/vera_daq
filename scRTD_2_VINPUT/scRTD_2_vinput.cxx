//
//  scRTD.cxx
//  
//
//  Created by Giacomo Gallina on 21/08/19.
//
//  OUTPUT
//
//  In "Hystory" in the ODB I display the 2 RTD temperature
//
//  NO SETTINGS ONLY VARIABLES
//
//  Usage: From Home/online/scRTD directory run the executabale ./scRTD
//  To compile it, use "make" from the same directory.
//
// Phydget Configuration:
//
// Channel 0: RTD
// Channel 1: RTD
// Channel 2: Pressure gauge Reader 



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "mfe.h"
// #include "mscb.h"
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <phidget22.h>


/*-- Globals -------------------------------------------------------*/
    
/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;    
    
//Declare an InterfaceKit handle
PhidgetTemperatureSensorHandle ifKit = 0;
PhidgetTemperatureSensorHandle ifKit_2 = 0;
PhidgetVoltageInputHandle ifKit_3 = 0;


PhidgetReturnCode prc; //Used to catch error codes from 1 Phidget function call

PhidgetReturnCode prc_2; //Used to catch error codes from 2 Phidget function call

PhidgetReturnCode prc_3; //Used to catch error codes from 2 Phidget function call   

int PROBLEM=0;
    
/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "scRTD";
/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;
    
/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;
    
/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;
    
/* maximum event size produced by this frontend */
INT max_event_size = 10000;
    
/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;
    
/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;
    
//char const mydevname[] = {"GPIB410"};
// HNDLE hDB, hDD, hSet, hControl;
// Add name that make sens when we know wich filters goes where.
    
    
int lmscb_fd;
HNDLE hmyFlag;
    
/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
    
INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);
void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
    
//Read data each fixed time
INT read_temperature_Phydget(char *pevent, INT off);

INT read_temperature_Phydget_2(char *pevent, INT off);

INT read_voltage_Phydget(char *pevent, INT off);
   
INT read_pressure_Phydget(char *pevent, INT off);
    
    
/*-- Equipment list ------------------------------------------------*/
        
      
        
EQUIPMENT equipment[] = {
  {
    "scRTD",             /* equipment name */
    {
      356, 0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_temperature_Phydget,       /* readout routine */
  },
  {
    "scRTD_2",             /* equipment name */
    {
      367, 0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_temperature_Phydget_2,       /* readout routine */
  },
  {
    "scVINPUT",             /* equipment name */
    {
      357, 0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_voltage_Phydget,       /* readout routine */
  },
  {
    "scPRESSURE_new",             /* equipment name */
    {
      359, 0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_pressure_Phydget,       /* readout routine */
  },
  {""}
};
        
        
        
void DisplayError(PhidgetReturnCode returnCode, const char* message) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  const char* error;
            
  fprintf(stderr, "Runtime Error -> %s\n\t", message);
            
  prc = Phidget_getErrorDescription(returnCode, &error);
  if (prc != EPHIDGET_OK) {
    DisplayError(prc, "Getting ErrorDescription");
    return;
  }
            
  fprintf(stderr, "Desc: %s\n", error);
            
  if (returnCode == EPHIDGET_WRONGDEVICE) {
    fprintf(stderr, "\tThis error commonly occurs when the Phidget function you are calling does not match the class of the channel that called it.\n"
	    "\tFor example, you would get this error if you called a PhidgetVoltageInput_* function with a PhidgetDigitalOutput channel.");
  }
  else if (returnCode == EPHIDGET_NOTATTACHED) {
    fprintf(stderr, "\tThis error occurs when you call Phidget functions before a Phidget channel has been opened and attached.\n"
	    "\tTo prevent this error, ensure you are calling the function after the Phidget has been opened and the program has verified it is attached.\n");
  }
  else if (returnCode == EPHIDGET_NOTCONFIGURED) {
    fprintf(stderr, "\tThis error code commonly occurs when you call an Enable-type function before all Must-Set Parameters have been set for the channel.\n"
	    "\tCheck the API page for your device to see which parameters are labled \"Must be Set\" on the right-hand side of the list.");
  }
}
        
void ExitWithErrors(PhidgetHandle *chptr) {
  Phidget_delete(chptr);
  fprintf(stderr, "\nExiting with error(s)...\n");
  printf("Press ENTER to end program.\n");
  getchar();
  exit(1);
}
 
void CheckError(const PhidgetReturnCode returnCode, const char* message, PhidgetHandle *chptr) {
            
  if (returnCode != EPHIDGET_OK) {
    DisplayError(returnCode, message);
    ExitWithErrors(chptr);
  }
}
        
static void CCONV onAttachHandler(PhidgetHandle ph, void *ctx) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_DeviceClass deviceClass;
  const char* channelClassName;
  int32_t serialNumber;
  int32_t hubPort;
  int32_t channel;
            
  //If you are unsure how to use more than one Phidget channel with this event, we recommend going to
  //www.phidgets.com/docs/Using_Multiple_Phidgets for information
            
  printf("\nAttach Event: ");
  /*
   * Get device information and display it.
   */
  prc = Phidget_getDeviceSerialNumber(ph, &serialNumber);
  CheckError(prc, "Getting DeviceSerialNumber", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannel(ph, &channel);
  CheckError(prc, "Getting Channel", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannelClassName(ph, &channelClassName);
  CheckError(prc, "Getting Channel Class Name", (PhidgetHandle *)&ph);
            
  prc = Phidget_getDeviceClass(ph, &deviceClass);
  CheckError(prc, "Getting Device Class", (PhidgetHandle *)&ph);
            
  if (deviceClass == PHIDCLASS_VINT) {
    prc = Phidget_getHubPort(ph, &hubPort);
    CheckError(prc, "Getting HubPort", (PhidgetHandle *)&ph);
                
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Hub Port: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, hubPort, channel);
  } else { //Not VINT
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, channel);
  }
            
  /*
   *       Set the DataInterval inside of the attach handler to initialize the device with this value.
   *       DataInterval defines the minimum time between TemperatureChange events.
   *       DataInterval can be set to any value from MinDataInterval to MaxDataInterval.
   */
            
  // printf("\tSetting RTD Type to 100 Ohm\n");
  // PhidgetTemperatureSensor_setRTDType((PhidgetTemperatureSensorHandle)ph, RTD_TYPE_PT100_3850);
            
  // ss_sleep(2000);
            
  // printf("\tSetting wire connections to 2 wires \n");
  // PhidgetTemperatureSensor_setRTDWireSetup((PhidgetTemperatureSensorHandle)ph, RTD_WIRE_SETUP_2WIRE);
            
  // ss_sleep(2000);
  // /*
  // //Trigger always when the temperature change
  // printf("\tSetting always ON trigger \n");
  // PhidgetTemperatureSensor_setTemperatureChangeTrigger((PhidgetTemperatureSensorHandle)ph, 0);
            
  // ss_sleep(2000);
  // */
  // //Set the trigger to the minimum value
  // printf("\tSetting DataInterval to 1000ms\n");
  // prc = PhidgetTemperatureSensor_setDataInterval((PhidgetTemperatureSensorHandle)ph, 1000);
  // CheckError(prc, "Setting DataInterval", (PhidgetHandle *)&ph);


            
}

void SetParameters(PhidgetHandle ph){
  printf("\tSetting RTD Type to 100 Ohm\n");
  PhidgetTemperatureSensor_setRTDType((PhidgetTemperatureSensorHandle)ph, RTD_TYPE_PT100_3850);
            
  ss_sleep(2000);
            
  printf("\tSetting wire connections to 2 wires \n");
  PhidgetTemperatureSensor_setRTDWireSetup((PhidgetTemperatureSensorHandle)ph, RTD_WIRE_SETUP_2WIRE);
            
  ss_sleep(2000);
  /*
  //Trigger always when the temperature change
  printf("\tSetting always ON trigger \n");
  PhidgetTemperatureSensor_setTemperatureChangeTrigger((PhidgetTemperatureSensorHandle)ph, 0);
            
  ss_sleep(2000);
  */
  //Set the trigger to the minimum value
  printf("\tSetting DataInterval to 1000ms\n");
  prc = PhidgetTemperatureSensor_setDataInterval((PhidgetTemperatureSensorHandle)ph, 1000);
  CheckError(prc, "Setting DataInterval", (PhidgetHandle *)&ph);
}

//VINT                                                                                                                      
static void CCONV onAttachHandler_VINT(PhidgetHandle ph, void *ctx) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call                               
  Phidget_DeviceClass deviceClass;
  const char* channelClassName;
  int32_t serialNumber;
  int32_t hubPort;
  int32_t channel;

  //If you are unsure how to use more than one Phidget channel with this event, we recommend going to              
  //www.phidgets.com/docs/Using_Multiple_Phidgets for information                                                  

  printf("\nAttach Event: ");
  /*                                                                                                               
   * Get device information and display it.                                                                        
   */
  prc = Phidget_getDeviceSerialNumber(ph, &serialNumber);
  CheckError(prc, "Getting DeviceSerialNumber", (PhidgetHandle *)&ph);

  prc = Phidget_getChannel(ph, &channel);
  CheckError(prc, "Getting Channel", (PhidgetHandle *)&ph);

  prc = Phidget_getChannelClassName(ph, &channelClassName);
  CheckError(prc, "Getting Channel Class Name", (PhidgetHandle *)&ph);

  prc = Phidget_getDeviceClass(ph, &deviceClass);
  CheckError(prc, "Getting Device Class", (PhidgetHandle *)&ph);

  if (deviceClass == PHIDCLASS_VINT) {
    prc = Phidget_getHubPort(ph, &hubPort);
    CheckError(prc, "Getting HubPort", (PhidgetHandle *)&ph);

    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Hub Port: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, hubPort, channel);
  } else { //Not VINT                                                                                              
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, channel);
  }

}




        
static void CCONV onDetachHandler(PhidgetHandle ph, void *ctx) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_DeviceClass deviceClass;
  const char* channelClassName;
  int32_t serialNumber;
  int32_t hubPort;
  int32_t channel;
            
  //If you are unsure how to use more than one Phidget channel with this event, we recommend going to
  //www.phidgets.com/docs/Using_Multiple_Phidgets for information
            
  printf("\nDetach Event: ");
  /*
   * Get device information and display it.
   */
  prc = Phidget_getDeviceSerialNumber(ph, &serialNumber);
  CheckError(prc, "Getting DeviceSerialNumber", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannel(ph, &channel);
  CheckError(prc, "Getting Channel", (PhidgetHandle *)&ph);
            
  prc = Phidget_getChannelClassName(ph, &channelClassName);
  CheckError(prc, "Getting Channel Class Name", (PhidgetHandle *)&ph);
            
  prc = Phidget_getDeviceClass(ph, &deviceClass);
  CheckError(prc, "Getting Device Class", (PhidgetHandle *)&ph);
            
  if (deviceClass == PHIDCLASS_VINT) {
    prc = Phidget_getHubPort(ph, &hubPort);
    CheckError(prc, "Getting HubPort", (PhidgetHandle *)&ph);
                
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Hub Port: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, hubPort, channel);
  } else { //Not VINT
    printf("\n\t-> Channel Class: %s\n\t-> Serial Number: %d\n\t-> Channel %d\n\n", channelClassName, serialNumber, channel);
  }
}

        
static void CCONV onErrorHandler(PhidgetHandle ph, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString) {
            
  fprintf(stderr, "[Phidget Error Event] -> %s (%d)\n", errorString, errorCode);
}

        
        
void CheckOpenError(PhidgetReturnCode e, PhidgetHandle *chptr) {
  PhidgetReturnCode prc; //Used to catch error codes from each Phidget function call
  Phidget_ChannelClass channelClass;
  int isRemote;
            
  if (e == EPHIDGET_OK)
    return;
            
  DisplayError(e, "Opening Phidget Channel");
  if (e == EPHIDGET_TIMEOUT) {
    fprintf(stderr, "\nThis error commonly occurs if your device is not connected as specified, "
	    "or if another program is using the device, such as the Phidget Control Panel.\n\n"
	    "If your Phidget has a plug or terminal block for external power, ensure it is plugged in and powered.\n");
                
    prc = Phidget_getChannelClass(*chptr, &channelClass);
    CheckError(prc, "Getting ChannelClass", chptr);
                
    if (channelClass != PHIDCHCLASS_VOLTAGEINPUT
	&& channelClass != PHIDCHCLASS_VOLTAGERATIOINPUT
	&& channelClass != PHIDCHCLASS_DIGITALINPUT
	&& channelClass != PHIDCHCLASS_DIGITALOUTPUT) {
      fprintf(stderr, "\nIf you are trying to connect to an analog sensor, you will need to use the "
	      "corresponding VoltageInput or VoltageRatioInput API with the appropriate SensorType.\n");
    }
                
    prc = Phidget_getIsRemote(*chptr, &isRemote);
    CheckError(prc, "Getting IsRemote", chptr);
                
    if (isRemote)
      fprintf(stderr, "\nEnsure the Phidget Network Server is enabled on the machine the Phidget is plugged into.\n");
  }
            
  ExitWithErrors(chptr);
}
        
      
//----------------------------------------------------------------
        
        
        
/*-- Sequencer callback info  --------------------------------------*/
    
    
void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("ODB Settings are only for display properties\n");

}
    
    

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
        
        
        
  /*Midas inizialization */
        
  // char  mscbstr[64];
  // int status = 1, size;
  // char set_str[80];

  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */
  //printf("1\n");
        
  //cm_get_experiment_database(&hDB, NULL); //E' giusto? posso farlo con solo variabili??? reset variabili in ODB
        
        
  //-----------------------------------------------------------------------------------------------
        

  //General Board Information
        
  int32_t serialNumber=561108;//S/N  Phydget Hub
  int32_t hubPort_phydget_1=0;
  int32_t hubPort_phydget_2=1;
  int32_t hubPort_phydget_3=2;
  int32_t channel=0;
        
        
  printf("\n\n\
               **********************************************************\n\
               *                                                        *\n\
               *                   MIDAS PHYDGET INTERFACE              *\n\
               *                                                        *\n\
               *         Management of temperature RTD Phydget board     *\n\
               *                   for nEXO experiment                  *\n\
               *                                                        *\n\
               *                                                        *\n\
               **********************************************************\n\n");
        
        
  printf("NOTE: The Hub parameters are coded IN !! ....\n\n");
  printf("If you change HUB or channel code must be changed ....\n\n");
        
        
  printf("Starting initialization....\n\n");

  printf("PHIDGET BOARD initialization....\n");
        
  /*Phidget initialization*/
        
  //create the InterfaceKit object 1
  PhidgetTemperatureSensor_create(&ifKit);
        
  //create the InterfaceKit object 2
  PhidgetTemperatureSensor_create(&ifKit_2);

  //create the InterfaceKit object 3                                                                                                                              
  PhidgetVoltageInput_create(&ifKit_3);
        
  //Open the device -1
        
  prc = Phidget_setDeviceSerialNumber((PhidgetHandle)ifKit, serialNumber);
  CheckError(prc, "Setting DeviceSerialNumber", (PhidgetHandle *)&ifKit);
        
  prc = Phidget_setHubPort((PhidgetHandle)ifKit, hubPort_phydget_1);
  CheckError(prc, "Setting HubPort", (PhidgetHandle *)&ifKit);
        
  prc = Phidget_setChannel((PhidgetHandle)ifKit, channel);
  CheckError(prc, "Setting Channel", (PhidgetHandle *)&ifKit);
        
	
  //Open the device -2
              
  prc_2 = Phidget_setDeviceSerialNumber((PhidgetHandle)ifKit_2, serialNumber);
  CheckError(prc_2, "Setting DeviceSerialNumber", (PhidgetHandle *)&ifKit_2);
              
  prc_2 = Phidget_setHubPort((PhidgetHandle)ifKit_2, hubPort_phydget_2);
  CheckError(prc_2, "Setting HubPort", (PhidgetHandle *)&ifKit_2);
              
  prc_2 = Phidget_setChannel((PhidgetHandle)ifKit_2, channel);
  CheckError(prc_2, "Setting Channel", (PhidgetHandle *)&ifKit_2);
        
        
  //Open the device -3 


  prc_3 = Phidget_setDeviceSerialNumber((PhidgetHandle)ifKit_3, serialNumber);
  CheckError(prc_3, "Setting DeviceSerialNumber", (PhidgetHandle *)&ifKit_3);

  prc_3 = Phidget_setHubPort((PhidgetHandle)ifKit_3, hubPort_phydget_3);
  CheckError(prc_3, "Setting HubPort", (PhidgetHandle *)&ifKit_3);

  prc_3 = Phidget_setChannel((PhidgetHandle)ifKit_3, channel);
  CheckError(prc_3, "Setting Channel", (PhidgetHandle *)&ifKit_3);






  /*                                                                                                                                                                                       
   * Add event handlers before calling open so that no events are missed.                                                                                                                   
   */

  //Device 1
  printf("\n--------------------------------------\n");
  printf("\nSetting OnAttachHandler Board 1...\n");
  prc = Phidget_setOnAttachHandler((PhidgetHandle)ifKit, onAttachHandler, NULL);
  CheckError(prc, "Setting OnAttachHandler", (PhidgetHandle *)&ifKit);
        
  printf("Setting OnDetachHandler Board 1...\n");
  prc = Phidget_setOnDetachHandler((PhidgetHandle)ifKit, onDetachHandler, NULL);
  CheckError(prc, "Setting OnDetachHandler", (PhidgetHandle *)&ifKit);
        
  printf("Setting OnErrorHandler Board 1...\n");
  prc = Phidget_setOnErrorHandler((PhidgetHandle)ifKit, onErrorHandler, NULL);
  CheckError(prc, "Setting OnErrorHandler", (PhidgetHandle *)&ifKit);
        
        
  //Device 2
  printf("\n--------------------------------------\n");
  printf("\nSetting OnAttachHandler Board 2...\n");
  prc_2 = Phidget_setOnAttachHandler((PhidgetHandle)ifKit_2, onAttachHandler, NULL);
  CheckError(prc_2, "Setting OnAttachHandler", (PhidgetHandle *)&ifKit_2);
             
  printf("Setting OnDetachHandler Board 2...\n");
  prc_2 = Phidget_setOnDetachHandler((PhidgetHandle)ifKit_2, onDetachHandler, NULL);
  CheckError(prc_2, "Setting OnDetachHandler", (PhidgetHandle *)&ifKit_2);
             
  printf("Setting OnErrorHandler Board 2...\n");
  prc_2 = Phidget_setOnErrorHandler((PhidgetHandle)ifKit_2, onErrorHandler, NULL);
  CheckError(prc_2, "Setting OnErrorHandler", (PhidgetHandle *)&ifKit_2);
        
        

  //Device 2                                                                                                                                                       
  printf("\n--------------------------------------\n");
  printf("\nSetting OnAttachHandler Board 3...\n");
  prc_3 = Phidget_setOnAttachHandler((PhidgetHandle)ifKit_3, onAttachHandler_VINT, NULL);
  CheckError(prc_3, "Setting OnAttachHandler", (PhidgetHandle *)&ifKit_3);

  printf("Setting OnDetachHandler Board 3...\n");
  prc_3 = Phidget_setOnDetachHandler((PhidgetHandle)ifKit_3, onDetachHandler, NULL);
  CheckError(prc_3, "Setting OnDetachHandler", (PhidgetHandle *)&ifKit_3);

  printf("Setting OnErrorHandler Board 3...\n");
  prc_3 = Phidget_setOnErrorHandler((PhidgetHandle)ifKit_3, onErrorHandler, NULL);
  CheckError(prc_3, "Setting OnErrorHandler", (PhidgetHandle *)&ifKit_3);




  //This call may be harmlessly removed
  //PrintEventDescriptions();
        
  //printf("Setting OnTemperatureChangeHandler...\n");
  //prc = PhidgetTemperatureSensor_setOnTemperatureChangeHandler(ifKit, onTemperatureChangeHandler, NULL);
  //CheckError(prc, "Setting OnTemperatureChangeHandler", (PhidgetHandle *)&ifKit);
        
  /*
   * Open the channel with a timeout
   */
  printf("Opening and Waiting for Attachment board 1...\n");
  prc = Phidget_openWaitForAttachment((PhidgetHandle)ifKit, 5000);
  CheckOpenError(prc, (PhidgetHandle *)&ifKit);

  SetParameters((PhidgetHandle)ifKit);
        
  printf("Opening and Waiting for Attachment board 2...\n");
  prc_2 = Phidget_openWaitForAttachment((PhidgetHandle)ifKit_2, 5000);
  CheckOpenError(prc_2, (PhidgetHandle *)&ifKit_2);
        
  SetParameters((PhidgetHandle)ifKit_2);

  printf("Opening and Waiting for Attachment board 3...\n");
  prc_3 = Phidget_openWaitForAttachment((PhidgetHandle)ifKit_3, 5000);
  CheckOpenError(prc_3, (PhidgetHandle *)&ifKit_3);

  printf("PHIDGET BOARD initialization....DONE\n\n");
        
        
   
  return FE_SUCCESS;
        
}
    
/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  printf("Closing...\n");
  //CPhidget_close((CPhidgetHandle)ifKit);
  //CPhidget_delete((CPhidgetHandle)ifKit);
        
        
  return SUCCESS;
        
}
    
/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  /*
    if(status != DB_SUCCESS)
    {
    cm_msg(MERROR,"BOR","cannot get value");
    return FE_ERR_ODB;
    }
  */
        
  return SUCCESS;
}
    
/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
        
  return SUCCESS;
}
    
/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}
    
/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}
    
/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}
    
/*------------------------------------------------------------------*/
// Readout routines for different events
/*-- Trigger event routines ----------------------------------------*/
    
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;
        
  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
	return lam;
  }
  return 0;
}
    
/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}
    
/*-- event --------------------------------------------------*/
    

INT read_temperature_Phydget(char *pevent, INT off) {
        
        
        
  bk_init32(pevent);
        
  double *pdata;
  bk_create(pevent, "TRTD", TID_DOUBLE, (void **)&pdata);
        
  double temp;
  std::cout << "Temperature_Phydget 1: ";
        
  PhidgetTemperatureSensor_getTemperature((PhidgetTemperatureSensorHandle)ifKit, &temp);
        
  std::cout << " " << temp;
  *pdata++ =  temp;
        
  std::cout << std::endl;
        
  bk_close(pevent,pdata);
        
        
  return bk_size(pevent);
}
    


INT read_temperature_Phydget_2(char *pevent, INT off) {
        
        
        
  bk_init32(pevent);
        
  double *pdata;
  bk_create(pevent, "TR2D", TID_DOUBLE, (void **)&pdata);
        
  double temp;
  std::cout << "Temperature_Phydget 2: ";
        
  PhidgetTemperatureSensor_getTemperature((PhidgetTemperatureSensorHandle)ifKit_2, &temp);
        
  std::cout << " " << temp;
  *pdata++ =  temp;
        
  std::cout << std::endl;
        
  bk_close(pevent,pdata);
        
        
  return bk_size(pevent);
}



INT read_voltage_Phydget(char *pevent, INT off) {
  bk_init32(pevent);

  double *pdata;
  bk_create(pevent, "VINT", TID_DOUBLE, (void **)&pdata);

  double Voltage;


  PhidgetVoltageInput_getVoltage((PhidgetVoltageInputHandle)ifKit_3, &Voltage);


  *pdata++ =  Voltage;


  bk_close(pevent,pdata);


  return bk_size(pevent);


}


INT read_pressure_Phydget(char *pevent, INT off) {


  double pressure;


  bk_init32(pevent);

  double *pdata;
  bk_create(pevent, "PVIN", TID_DOUBLE, (void **)&pdata);

  double Voltage;


  PhidgetVoltageInput_getVoltage((PhidgetVoltageInputHandle)ifKit_3, &Voltage);

  std::cout << "Voltage: "<<Voltage<<" V,"<<" Pressure ";


  pressure=pow(10.0,1.667*Voltage-11.33);


  *pdata++ =  pressure;
  std::cout << " " << pressure<<" mbar"<<std::endl;

  bk_close(pevent,pdata);


  return bk_size(pevent);


}










