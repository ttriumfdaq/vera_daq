add_executable(fePowerSwitch fePowerSwitch.cxx)
target_include_directories(fePowerSwitch PRIVATE ${INC_PATH} ${CMAKE_SOURCE_DIR}/tcp)
target_link_libraries(fePowerSwitch fetcp ${LIBS})

install(TARGETS fePowerSwitch)
