# Prerequisites

[CMake](https://cmake.org/) v.3.16.3

[MIDAS](https://midas.triumf.ca) tag: midas-2020-12-a

[Phidgets](https://www.phidgets.com/docs/OS_-_Linux) Phidget22 libraries

[CAEN USB driver](https://www.caen.it/?downloadfile=6475) v.1.5.2

# Build the package

```
git clone https://bitbucket.org/ttriumfdaq/vera_daq.git ~/online
cd ~/online
mkdir build
cmake ..
cmake --build . -- install
```

The MIDAS frontents are installed in `~/online/bin`.

# ser2net configuration

Service running: `/usr/sbin/ser2net -c /etc/ser2net.conf -P /run/ser2net.pid`

The current (working) configuration for the power switch [Synaccess Networks NP-0801DUG2](https://www.synaccess-net.com/np-0801du) is in `/etc/ser2net.conf`

```
3003:raw:600:/dev/ttyACM0:9600 8DATABITS NONE 1STOPBIT -RTSCTS
```

# Quick start

If this is not in `bashrc`, define the required enviroment variable 

```
export MIDAS_EXPTAB=~/online/exptab
```

Then, the DAQ can be started as

```
cd ~/online/bin
./start_daq.sh
```
