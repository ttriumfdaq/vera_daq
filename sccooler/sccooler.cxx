//
//  sccooler.cxx
//
//
//  Created by Giacomo Gallina
//  Modified by Lars Martin
//
//  This MIDAS file manage the COOLER for nEXO experiment to manage the COOLER
//  with the USB interface
//
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "midas.h"
#include "mfe.h"
//#include "mscb.h"
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <stdexcept>      // std::invalid_argument


/*-- Globals -------------------------------------------------------*/

/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;    
    
//GLOBAL VARIABLES for cooler communication


//    int NPLC_value = 100; //range: 10us - 100PLC(60Hz), PLC = nplc/power line frequency, NPLC = time * Power line frequency, PLF =
int sleep_time = 3; // in seconds, ideally 2*NPLC, but try other ranges NPLC * 1/60 = integration time (s
int buf_size = 4095;

std::fstream device;
//    char devname[] = "/dev/usbtmc0";

char devname[64] = "/dev/ttyACM";
// char devname[] = "/home/exo/online/sccooler_new/testdev.log";
const std::string devId = "MK2000VCP";


std::string str;
int n;
char buf[4096];



/* The frontend name (client name) as seen by other MIDAS clients   */
char const *frontend_name = "scCooler";

/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;


#define COOLER_SETTINGS_STR(_name) char const *_name[] = {	\
        "Final_temperature = FLOAT : 20",                       \
        "HOLD_MODE = INT : 0",					\
        "Actual_temp = FLOAT : 20",                             \
        "",                                                     \
        NULL }

typedef struct {
    float Final_temperature;
    float Actual_temp;
    int  HOLD_MODE;
} COOLER_SETTINGS;


COOLER_SETTINGS COOLER_settings;

int lmscb_fd;
HNDLE hmyFlag;

/*-- Function declarations -----------------------------------------*/

//MIDAS FUNCTION
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

INT read_temperature_COOLER(char *pevent, INT off);
INT read_Cooler_io_event(char *pevent, INT off);
void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info);

EQUIPMENT equipment[] = {

    { "SCCOOLER",                 /* equipment name */
      {
          43,
          0,     /* event ID, trigger mask */
          "SYSTEM",              /* event buffer */
          EQ_PERIODIC ,      /* equipment type */
          0,     /* event source */
          "MIDAS",                /* format */
          TRUE,                   /* enabled */
          RO_ALWAYS,             /* read always */
          5000,                    /* read every 5 sec */
          0,                      /* stop run after this event limit */
          0,                      /* number of sub events */
          0,                      /* don't log history */
          "", "", "",
      },
      read_Cooler_io_event,       /* readout routine */
    },
    {
        "COOLER_Temperature",             /* equipment name */
        {
            44,
            0,            /* event ID, corrected with feIndex, trigger mask */
            "SYSTEM",               /* event buffer */
            EQ_PERIODIC,            /* equipment type */
            0,                      /* event source */
            "MIDAS",                /* format */
            TRUE,                   /* enabled */
            RO_ALWAYS |    /* read when running and on transitions */
            RO_ODB,                 /* and update ODB */
            1000,                   /* read every 1 sec */
            0,                      /* stop run after this event limit */
            0,                      /* number of sub events */
            1,                      /* log history */
            "", "", ""
        },
        read_temperature_COOLER,       /* readout routine */
    },
    {""}
};

//----------------------------------------------------------------

std::string communicate(std::string command)
{
    std::string response;
    if(device.is_open())
        device.close();

    if(!device.is_open()) device.open(devname);

    if(!device.is_open()){
        printf("error\n");
        return response;
    }

    device.clear();
    if(device.rdbuf()->in_avail() > 0){
        char buf[1024];
        device.getline(buf, 1024);
    }

    std::cout << "Sending: >" << command << '<' << std::endl;
    device << command << "\r\n";
    device.flush();
    if(command.back() == '?'){
        usleep(500000);
        if(device.rdbuf()->in_avail() > 0){
            char buf[256];
            device.getline(buf, 256);
            std::istringstream iss(buf);
            iss >> response;
            std::cout << "Response: " << buf << std::endl;
            assert(response.find("ERROR") == std::string::npos);
        } else {
            std::cout << "No Response" << std::endl;
        }
    }
    device.close();
    return response;
}

#define CSTATUS_IDLE_RST 0
#define CSTATUS_HOLD 1
#define CSTATUS_RAMP 2
#define CSTATUS_PAUSE 3
#define CSTATUS_PROFILE 4
#define CSTATUS_IDLE_STOP 5

int get_cooler_status(){
    int status = -1;
    char cmd[] = "TEMP:STAT?";
    std::string response = communicate(cmd);
    if(!response.size()) response = communicate(cmd);
    try {			// try block to prevent crash due to malformed response
        status = std::stoi(response);
    }
    catch(std::invalid_argument const&) {
        std::cerr << "Couldn't extract int from response: >" << response << "<" << std::endl;
    }
    return status;
}
/*-- Sequencer callback info  --------------------------------------*/


//STOP CONTROLLER
void seq_Stop_Cooling_2(void)
{


    printf("Stopping Cooler...\n\n");

    str = "TEMP:STOP";

    // device << str << std::endl;
    communicate(str);
    int status = get_cooler_status();
    if(status != CSTATUS_IDLE_STOP){
        cm_msg(MERROR,"FE","Couldn't stop cooling first try, status = %d, trying again", status);
        communicate(str);
        status = get_cooler_status();
    }

    if(status == CSTATUS_IDLE_STOP)
        cm_msg(MINFO,"FE","Cooler stopped");
    else
        cm_msg(MERROR,"FE","Couldn't stop cooling, status = %d", status);

}


//ENEBLE HOLD MODE WITH DESIDERED FINAL TEMPERATURE
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info)
{


    float temp_to_reach;
    int size,status;
    int run;

    char str_old[2][512];

    size = sizeof(run);
    status = db_get_value(hDB, 0,"/Equipment/scCOOLER/Settings/HOLD_MODE", &run, &size, TID_INT, FALSE);
    if(status != SUCCESS)
        cm_msg(MERROR,"FE","Trouble getting Hold mode ODB entry, status = %d", status);



    if(run>1 || run<0){

        printf("Wrong value, only permitted values are 0 and 1\n");


    }else{


        if(run==0){
            seq_Stop_Cooling_2();
            return;

        }


        size = sizeof(temp_to_reach);
        status = db_get_value(hDB, 0,"/Equipment/scCOOLER/Settings/Final_temperature", &temp_to_reach, &size, TID_FLOAT, FALSE);




        strcpy(str_old[0], "TEMP:HOLD ");

        char settaggio[64];

        printf("HOLDING MODE ENABLED, reaching desidered temperature...\n\n");

        sprintf(settaggio,"%f",temp_to_reach);

        strcat(str_old[0],settaggio);

        // //Write on Standard Output
        // puts(str_old[0]);

        //Convert character array to string
        std::string str_new(str_old[0]);

        std::cout<<"New String: "<<str_new << std::endl;

        //Send to device
        // device << str_new << std::endl;
        // str_new.resize(str_new.size()-1); // remove extra newline character
        communicate(str_new);
        int cstatus = get_cooler_status();
        if(cstatus != CSTATUS_HOLD){
            cm_msg(MERROR,"FE","Couldn't start hold mode first try, status = %d, trying again", cstatus);
            communicate(str_new);
            cstatus = get_cooler_status();
        }

        if(cstatus == CSTATUS_HOLD)
            cm_msg(MINFO,"FE","Cooler hold mode");
        else
            cm_msg(MERROR,"FE","Couldn't start hold mode, status = %d", cstatus);

    }

}




/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{



    printf("\n\n\
               **********************************************************\n\
               *                                                        *\n\
               *                  MIDAS  COOLER INTERFACE               *\n\
               *                                                        *\n\
               *                Management of COOLING stage             *\n\
               *                    for nEXO experiment                 *\n\
               *                                                        *\n\
               *                                                        *\n\
               **********************************************************\n\n");



    //MIDAS
    int status = 1, size;
    char set_str[80];


    COOLER_SETTINGS_STR(COOLER_settings_str);

    /* hardware initialization */
    /* print message and return FE_ERR_HW if frontend should not be started */

    cm_get_experiment_database(&hDB, NULL);

    /* Map /equipment/Trigger/settings for the sequencer */
    sprintf(set_str, "/Equipment/%s/Settings", "scCOOLER");


    /* create PARAM settings record */
    status = db_create_record(hDB, 0, set_str, strcomb(COOLER_settings_str));
    if (status != DB_SUCCESS)  return FE_ERR_ODB;
    // status = db_find_key(hDB, 0, set_str, &hSet);

    // if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);


    //Opening COOLER

    printf("Opening COOLER device using USBTMC connection ...\n");

    for(int devnum = 0; devnum < 16; devnum++){
        char dn[128];
        sprintf(dn, "%s%d", devname, devnum);
        printf("Trying Device=%s\n",dn);

        device.open(dn);
        if(device.is_open()){
            device << "*IDN?" << std::endl;
            sleep(sleep_time);

            device.getline(buf, buf_size);
            n = device.gcount();
            std::cout << "Received " << n << " characters, : " << buf << std::endl;
            std::string idstring = buf;
            if(idstring.find(devId) == std::string::npos){
                device.close();
            } else {
                std::cout << "Success" << std::endl;
                strcpy(devname, dn);
                break;
            }
        }
    }

    if(! device.is_open()){
        cm_msg(MERROR, "sccooler", "Failed to connect to device.");
        return 0;
    }

    printf("Loading default values in COOLER.....\n\n");



    //Set ROOM temperature as initial temperature
    float flag=20.0;
    db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Final_temperature", &flag, sizeof(flag), 1, TID_FLOAT);

    //Set Cooler state as off
    int flag_3=0;
    db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/HOLD_MODE", &flag_3, sizeof(flag_3), 1, TID_INT);

    printf("Stop the cooler.....\n");


    printf("Loading default values DONE.....!\n\n");






    //-------------Hot link initialization --------------//

    printf("HOT links creation....\n");


    //Hold final temperature
    status = db_find_key(hDB, 0, "/Equipment/scCOOLER/Settings/HOLD_MODE", &hmyFlag);
    if (status == DB_SUCCESS) {
        /* Enable hot-link on settings/ of the equipment */
        if (hmyFlag) {
            size = sizeof(BOOL);
            if ((status = db_open_record(hDB, hmyFlag, &(COOLER_settings.HOLD_MODE)
                                         , size, MODE_READ
                                         , seq_HOLD_FINAL_TEMPERATURE, NULL)) != DB_SUCCESS)
                return status;
        }
    }else{
        cm_msg(MERROR, "feSCCOOLER", "cannot access output");
    }




    printf("HOT links creation....DONE\n\n");


    return FE_SUCCESS;

}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{

    printf("Closing...\n");

    //Closing serial PORT
    //        RS232_CloseComport(cport_nr);

    return SUCCESS;

}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

    return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
    /* if frontend_call_loop is true, this routine gets called when
       the frontend is idle or once between every event */
    ss_sleep(100);
    return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events


/*-- Trigger event routines ----------------------------------------*/


INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
    int i;
    DWORD lam;

    for (i = 0; i < count; i++) {
        lam = 1;
        if (lam & LAM_SOURCE_STATION(source))
            if (!test)
                return lam;
    }
    return 0;
}



/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
    switch (cmd) {
    case CMD_INTERRUPT_ENABLE:
        break;
    case CMD_INTERRUPT_DISABLE:
        break;
    case CMD_INTERRUPT_ATTACH:
        break;
    case CMD_INTERRUPT_DETACH:
        break;
    }
    return SUCCESS;
}

/*-- event --------------------------------------------------*/


INT read_Cooler_io_event(char *pevent, INT off)
{
    float *pfdata;
    /* init bank structure */
    bk_init(pevent);

    /* create Phidget bank */
    bk_create(pevent, "COOL", TID_FLOAT, (void **) &pfdata);


    bk_close(pevent, pfdata);


    return 0;

    printf("event size %d\n", bk_size(pevent));

    return bk_size(pevent);
}




//ROUTINE TO MANAGE THE READING OF TEMPERATURE


INT read_temperature_COOLER(char *pevent, INT off) {

    double *pdata;
    float temp;


    bk_init32(pevent);


    bk_create(pevent, "CTEM", TID_DOUBLE, (void **)&pdata);

    str = "TEMP:CTEM?";

    // device << str << std::endl;
    std::string response = communicate(str);
    if(!response.size()) response = communicate(str); // FIXME: Somehow communication doesn't work half the time.

    if(response.size()){
        try {			// try block to prevent crash due to malformed response
            temp = std::stof(response);
            printf("Temperature wrote in MIDAS: %e\n\n",temp);


            float flag=temp;
            db_set_value(hDB,0,"/Equipment/scCOOLER/Settings/Actual_temp", &flag, sizeof(flag), 1, TID_FLOAT);
            *pdata++ =  temp;
        }
        catch(std::invalid_argument const&) {
            std::cerr << "Couldn't extract float from response: >" << response << "<" << std::endl;
        }
    }
    // if(!device.good()){
    //   std::string err;
    //   if(device.eof()) err = "EOF";
    //   if(device.fail()) err = "FAIL";
    //   if(device.bad()) err = "BAD";
    //   std::cerr << "Device not good after write!: " << err << std::endl;
    //   // device.clear();
    //   device.close();
    //   device.open(devname);
    //   device << str << std::endl;
    // }

    sleep(sleep_time);	// FIXME: what is this for?

    // device.getline(buf, buf_size);
    // n = device.gcount();
    // std::cout << "Received " << n << " characters, Temperature " << buf << std::endl;

    // temp=atof((char*)buf);

    // device >> temp;
    // if(!device.good()){
    //   std::string err;
    //   if(device.eof()) err = "EOF";
    //   if(device.fail()) err = "FAIL";
    //   if(device.bad()) err = "BAD";
    //   std::cerr << "Device not good after read!: " << err << std::endl;
    //   // device.clear();
    //   device.close();
    //   device.open(devname);
    //   device << str << std::endl;
    // }


    bk_close(pevent,pdata);
    return bk_size(pevent);
}
