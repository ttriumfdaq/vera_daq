/********************************************************************\

  This is a MIDAS front-end for the DT5730B ---- USB Interface

  This front end is based on a previous versione developed by T. Lindner (April 2015)
 
  Created by Giacomo Gallina on 10/19.

  This code needs to be cleaned a little bit ....:)

\********************************************************************/


#define CAEN_VME
#define V1730_CODE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <time.h>
#include "midas.h"
#include "mfe.h"
// #include "mvmestd.h"
// #include "gefvme.h"

#include "CAENComm.h"

// #ifdef CAEN_VME
// #endif

#include "v1730Board.h"
#include "v1730drv.h"

#include "OdbV1730.h"

#define  EQ_NAME   "FEV1730USB"
#define  EQ_EVID   1
#define  EQ_TRGMSK 0x0100

/* Globals */
#define N_V1730 1

clock_t begin,end;

float total_seconds;

/* Hardware */
MVME_INTERFACE *myvme;
//uint32_t V1730_BASE[N_V1730] = {0x3230000};
uint32_t V1730_BASE[N_V1730] = {0x510000}; // try 24bit address

v1730Board *myv1730Board;

HNDLE hSet[N_V1730];
V1730_CONFIG_SETTINGS tsvc[N_V1730];
const char BankName[N_V1730][5]={"V730"};

// VMEIO definition

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char const *frontend_name = "feV1730Raw";
/* The frontend file name, don't change it */
char const *frontend_file_name = (char*)__FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;    
    
/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 32 * 34000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 2 * max_event_size + 10000;

/* VME base address */
int   v1730_handle[N_V1730];
DWORD reg;  
int  linRun = 0;
int  done=0, stop_req=0;

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
void interrupt_routine(void);
INT read_trigger_event(char *pevent, INT off);
INT read_buffer_level(char *pevent, INT off);
INT read_temperature(char *pevent, INT off);



/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {

  { EQ_NAME,                 /* equipment name */
    {
      EQ_EVID, EQ_TRGMSK,     /* event ID, trigger mask */
      "SYSTEM",              /* event buffer */
      EQ_POLLED ,      /* equipment type */
      LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      500,                    /* poll for 500ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",
    },
    read_trigger_event,       /* readout routine */
  },                                                                                
  {
    "V1730_Temperature",             /* equipment name */
    {
      100, 0x1000,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_temperature,       /* readout routine */
  },
  {""}
};

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/



/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  KEY key;

  printf("hseq = %d\n",hseq);
  printf("N_V1730 = %d\n",N_V1730);
  printf("odb ... Settings %x touched\n", hseq);
  printf("post trigger = %i\n",tsvc[0].post_trigger);          
  printf("threshold[0] = %i\n",tsvc[0].threshold[0]);          

  for (int b=0;b<N_V1730;b++) {
    if (hseq == hSet[b]) {
      db_get_key(hDB, hseq, &key);

      printf("odb ... Settings %s touched\n", key.name);     

      int size = sizeof(V1730_CONFIG_SETTINGS);
      int status;
      if ((status = db_get_record (hDB, hSet[b], &tsvc[b], &size, 0)) != DB_SUCCESS)
	return;
      printf("post trigger = %i\n",tsvc[b].post_trigger);          
    }
  }
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
    
    
    
    
    printf("\n\n\n\
           **********************************************************\n\
           *                                                        *\n\
           *                     MIDAS  DT5370B - USB               *\n\
           *                                                        *\n\
           *                Management of DT5370B stage             *\n\
           *                    for nEXO experiment                 *\n\
           *                                                        *\n\
           *                                                        *\n\
           **********************************************************\n\n");
    

    
  int size, status;
  char set_str[80];

  // Suppress watchdog for PICe for now
  cm_set_watchdog_params(FALSE, 0);

  //  setbuf(stdout, NULL);
  //  setbuf(stderr, NULL);
  printf("begin of Init\n");
  /* Book Setting space */
  V1730_CONFIG_SETTINGS_STR(v1730_config_settings_str);

  sprintf(set_str, "/Equipment/feV1730USB/Settings/V1730");
  status = db_create_record(hDB, 0, set_str, strcomb(v1730_config_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet[0]);
  if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(V1730_CONFIG_SETTINGS);
  if ((status = db_open_record(hDB, hSet[0], &(tsvc[0]), size, MODE_READ, seq_callback, NULL)) != DB_SUCCESS)
    return status;

  printf("New V1730 object\n");
  myv1730Board = new v1730Board();
  printf("Connecting\n");
  myv1730Board->Connect();
  printf("Reset Board\n");
  myv1730Board->WriteReg(V1730_SW_RESET, 1);
  printf("Done reset.\n");
  printf("Clear Board\n");
  myv1730Board->WriteReg(0xEF28,1);




  char ss_fw_datatype[256]= {"Module "};
  char str[64];
  // Firmware version check (0x1n8C)
  // read each AMC firmware version
  // [31:16] Revision date Y/M/DD
  // [15:8] Firmware Revision (X)
  // [7:0] Firmware Revision (Y)
  // eg 0x760C0103 is 12th June 07, revision 1.3
  int addr = 0;
  //int addr_2 = 0;
  DWORD version = 0;
  uint32_t prev_chan = 0;
  // Hardcode correct firmware verisons
  // Current release 3.4_0.11 Feb 2012
  // AMC FW: 0xd3280005, ROC FW: 0xd4110401
  //const uint32_t amc_fw_ver = 0xe7028802; // Update 27Nov2014
  const uint32_t amc_fw_ver = 0xe4140000; // Changed by TL (2015-05-12)
  const uint32_t roc_fw_ver = 0xe4280402; // Update 27Nov2014
  //myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);
  //myv1730Board->WriteReg(0xEF28, 1);
  // myv1730Board->WriteReg(0xEF24, 1);

  printf("Read AMC version number\n");
  // Somehow this doesn't work if we don't read the register first once
  


  // myv1730Board->ReadReg(0x108c,&version);
  //ss_sleep(1000);
  myv1730Board->ReadReg(0x108c,&version);


  for(int iCh=0;iCh<16;iCh++) {
    addr = 0x108c | (iCh << 8);
    myv1730Board->ReadReg(addr,&version);
    //printf("%i %x %x \n",iCh, addr,version);
    if((iCh != 0) && (prev_chan != version)) {
      cm_msg(MERROR, "Initialize","Error Channels have different AMC Firmware ");
    }
    prev_chan = version;
  }
  if(version != amc_fw_ver) {
    //commented
    cm_msg(MERROR,"Initialize","Incorrect AMC Firmware Version: 0x%08x", version);
  } else {
    sprintf(str, "AMC FW: 0x%x, ", version);
    strcat(ss_fw_datatype, str);
  }
  printf("AMC Firmware revision date: 20%i/%i/%i\n",
	 (version & 0xf0000000) >> 28,
	 (version & 0x0f000000) >> 24,
	 (version & 0x00ff0000) >> 16);
  
  // read ROC firmware revision
  // Format as above

  printf("Read ROC version number\n");
  myv1730Board->ReadReg(V1730_ROC_FPGA_FW_REV,&version);
  printf("Finished read ROC version number\n");
  switch (version) {
  case roc_fw_ver:
    sprintf(str, "ROC FW: 0x%x", version);
    strcat(ss_fw_datatype, str);
    break;
  default:
    //		cm_msg(MERROR,"Initialize","Incorrect ROC Firmware Version: 0x%08x", version);
    break;
  }
  printf("ROC Firmware revision date: 20%i/%i/%i\n",
	 (version & 0xf0000000) >> 28,
	 (version & 0x0f000000) >> 24,
	 (version & 0x00ff0000) >> 16);
  
  DWORD reg;
  myv1730Board->ReadReg(V1730_ACQUISITION_STATUS,&reg);
  sprintf(str, "Finished initialization, Acq Reg: 0x%x", reg); 
  strcat(ss_fw_datatype, str);
  cm_msg(MINFO, "Initialize", "%s", ss_fw_datatype);


  
  // Stop board
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);


  //--------------- End of Init cm_msg debug ----------------
  set_equipment_status(equipment[0].name, "Initialized", "#00ff00");

	//exit(0);
  printf("end of Init\n");
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  printf("End of exit\n");
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  int status;
  DWORD mask;
  DWORD nsamples;
    
  printf("Begin of Run configuration\n");

  printf("Cleaning Buffer....\n");
  myv1730Board->WriteReg(0xEF28,1);

  // Stop board
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);

  /* read Triggger settings */
  int nInitOk=0;
  for (int module=0; module<N_V1730; module++) {
    int size = sizeof(V1730_CONFIG_SETTINGS);
    if ((status = db_get_record (hDB, hSet[module], &tsvc[module], &size, 0)) != DB_SUCCESS)
      return status;
    
    // load setup config is != 0
    if (tsvc[module].setup != 0) {
      cm_msg(MERROR, "AcqInit", "setup != 0 not supported yet...");
    }else{ 
      
       printf(" Doing setup in Begin of RUN .... \n");
        
      // Reset ACQ Control
      myv1730Board->WriteReg(V1730_ACQUISITION_CONTROL, 0x00);
      
     // Acq mode 3:Reg
      myv1730Board->v1730_AcqCtl(tsvc[module].acq_mode);

        //--> Set the Buffer organization
         myv1730Board->WriteReg(V1730_BUFFER_ORGANIZATION , tsvc[module].buffer_organization);
        myv1730Board->ReadReg(V1730_BUFFER_ORGANIZATION, &mask);
        printf("Reading buffer organization 0x%x\n",mask);
      

	//ss_sleep(1000);
	//ss_sleep(1000);


        //--> Set the Custom size: If you want an event smaller than BUFFER use custom size otherwise
        //    set 0 on this register to disable it so you will use BUFFER_ORGANIZATION
        myv1730Board->WriteReg(V1730_CUSTOM_SIZE, tsvc[module].custom_size);
        myv1730Board->ReadReg(V1730_CUSTOM_SIZE, &nsamples);
        printf("Reading Custom Size = 0x%x\n",nsamples);
        
        if(nsamples==0){
            
            printf("Custom size disabled... using BUFFER ORGANIZATION\n");
            
        }
        

        //--> Channel Mask. Disable or enable single channels
        myv1730Board->WriteReg(V1730_CHANNEL_EN_MASK, tsvc[module].ch_mask);
        myv1730Board->ReadReg(V1730_CHANNEL_EN_MASK, &mask);
        printf("Reading Channel mask 0x%x\n",mask);


        /*-->GLOBAL TRIGGER MASK
         Set what effectively contributes to the trigger (software, externale etc...)
         The generation of a common acquisition trigger is based on different trigger sources
         (configurable at this register, address 0x810C==V1730_TRIG_SRCE_EN_MASK). See later for explanation
         */
        
        myv1730Board->WriteReg(V1730_TRIG_SRCE_EN_MASK, tsvc[module].trigger_source);
      
      
      // channel on ECL OUT for trigger
      myv1730Board->WriteReg(V1730_FP_TRIGGER_OUT_EN_MASK, tsvc[module].trigger_output);

        
        //--> Set POST TRIGGER SETTINGS
        myv1730Board->WriteReg(V1730_POST_TRIGGER_SETTING  , tsvc[module].post_trigger);
        myv1730Board->ReadReg(V1730_POST_TRIGGER_SETTING, &mask);
        printf("Reading Post trigger settings 0x%x\n",mask);

	//ss_sleep(1000);
	//ss_sleep(1000);

        
        //--> Set FULL OR ALMOST FULL BUFFER. The written value (ALMOST FULL LEVEL) represents
        //the number of buffers that must be full of data before to assert the BUSY signal.
        //If this register is set to 0, the ALMOST FULL is a FULL.
      myv1730Board->WriteReg(V1730_ALMOST_FULL_LEVEL , tsvc[module].almost_full);
      myv1730Board->ReadReg(V1730_ALMOST_FULL_LEVEL, &mask);
      printf("Reading Almost Full/Full level 0x%x\n",mask);
      

      // Buff occupancy on MON out
      myv1730Board->WriteReg(V1730_MONITOR_MODE, 0x3);  // buffer occupancy
      
      

   //-->Board confuguration ( Overlapping triggers, polarity self trigger) Channel registro : 0x8000, vedi appunti
      myv1730Board->WriteReg(V1730_CHANNEL_CONFIG,0x10010); 
      DWORD config;
      myv1730Board->ReadReg(V1730_CHANNEL_CONFIG, &config); 
      printf("Board configuration = 0x%x\n",config);
    
  
        //(??) Registri: 0x811C, 0x81A0
      // Setup Busy daisy chaining
      

      //Enable extended time stamp
      //myv1730Board->WriteReg(V1730_FP_IO_CONTROL,  0x00400104); // 0x100:enable new config, 0x4:LVDS I/O[3..0] output


      myv1730Board->WriteReg(V1730_FP_IO_CONTROL,  0x104); // 0x100:enable new config, 0x4:LVDS I/O[3..0] output OLD
      myv1730Board->WriteReg(V1730_FP_LVDS_IO_CRTL, 0x022); // 0x20: I/O[7..4] input mode nBusy/Veto (4:nBusy input)
                                                                                    // 0x02: I/O[3..0] output mode nBusy/Veto (1:Busy)
    
      
      DWORD temp;
    
       // -->ADC Calibration
        
        
        
      int addr;
      int addr_2 = 0;
      // This register poke does the ADC calibration
      myv1730Board->WriteReg(0X809C , 0);
      // Now we check to see when the calibration has finished.
      // by checking register 0x1n88.
      for (int i=0;i<16;i++) {
        addr = 0x1088 | (i << 8);
        myv1730Board->ReadReg(addr,&temp);
        printf("Channel (%i) Status: %x\n",i,temp);
        if((temp & 0x4) == 0x4){
          printf("waiting for ADC calibration to finish...\n");
          int j;
          for(j =0; j < 20; i++){
            sleep(1);
            printf("temp %x\n",temp);
            myv1730Board->ReadReg(addr,&temp);
            if((temp & 0x4) == 0x0){
              break;
            }
          }
          if(j < 19){
            printf("Took %i seconds to finish calibration\n",j+1);
          }else{
            printf("Calibration did not finish in %i seconds\n",j);
          }					
        }
      }
  

      //--> Set DC Offset of each channel
        
      //ss_sleep(1000);
      //ss_sleep(1000);

      	ss_sleep(1000);
        
      for (int i=0;i<16;i++) {

	addr_2 = V1730_CHANNEL_STATUS | (i << 8);
	myv1730Board->ReadReg(addr_2,&temp);

	printf("Board Status: STATUS[%i] = %d 0x%x \n", i, temp,addr_2);
	
        addr = V1730_CHANNEL_DAC | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].dac[i]);

	ss_sleep(1000);   
	
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d DAC[%i] = %d %d 0x%x \n", module, i, temp,tsvc[module].dac[i],addr);

	
      }
      ss_sleep(1000);      
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_DAC | (i << 8);
	myv1730Board->ReadReg(addr,&temp);
        printf("Second DAC readback (%d) DAC[%i] = %d  \n", module, i, temp);
      }


      //ss_sleep(1000);
      //ss_sleep(1000);


      //--> Set the input dynamic range of each channel
        
        
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_GAIN | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].gain[i]);
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d IDR[%i] = %d \n", module, i, temp);
      }
      
      //--> Set the channel threshold for each channel
        

      ss_sleep(1000);
      ss_sleep(1000);
        
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_THRESHOLD | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].threshold[i]);
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d Threshold[%i] = %d \n", module, i, temp);
      }
      
  
      // SW clock sync
      myv1730Board->WriteReg(0x813C , 1);
      
      
      // Set Board ID in the data stream!
      myv1730Board->WriteReg(V1730_BOARD_ID, 6);


      //NEW PIECES
    
      // SELF TRIGGER (1: internal trigger, 0: external trigger)
      int ST=1;
      
  
    //-->Global trigger Mask for self trigger


      if(ST==1){
      
	//reg=0x104000001;
      reg=0xF; //16/10
      myv1730Board->WriteReg(0x810C, reg); // Self trigger

      }
    

     //-->IDR
    
    reg=0x1;
	myv1730Board->WriteReg(0x1028, reg); //input range at 0.5 volts chan 0

	myv1730Board->WriteReg(0x1128, reg); //input range at 0.5 volts chan 1

	myv1730Board->WriteReg(0x1228, reg); //input range at 0.5 volts chan 2

	myv1730Board->WriteReg(0x1328, reg); //input range at 0.5 volts chan 3

	ss_sleep(1000);
        
    //-->DC Offset
        
	//reg=0x9000;//Old  DAQ value
	
	reg=0x1388;// unipolar negative signal
	//reg=0xF100;// unipolar positive signal


	//ss_sleep(1000);
	//ss_sleep(1000);
	//ss_sleep(1000);
	ss_sleep(1000);

	myv1730Board->WriteReg(0x1098, reg); // DAQ value

	ss_sleep(1000);   
	
	myv1730Board->WriteReg(0x1198, reg); // DAQ

	ss_sleep(1000);   
	
	myv1730Board->WriteReg(0x1298, reg); // DAQ

	ss_sleep(1000);   
	
	myv1730Board->WriteReg(0x1398, reg); // DAQ

	ss_sleep(1000);   
	
	myv1730Board->WriteReg(0x1498, reg); // DAQ

    //WAIT AFTER DC OFFSET IS SETTED
    ss_sleep(1000);
    ss_sleep(1000);
        
    //-->Self trigger routine

	if(ST==1){
    
    //reg=0x7; //16/10
    reg=0x5;
  	  
    myv1730Board->WriteReg(0x1084, reg); // self trigger
    myv1730Board->WriteReg(0x1184, reg); // self trigger
    myv1730Board->WriteReg(0x1284, reg); // self trigger
    myv1730Board->WriteReg(0x1384, reg); // self trigger
    
    myv1730Board->ReadReg(0x1084,&reg);
    printf("Self trigger value routine  0x1084 is : 0x%x\n",reg);
	
    //ss_sleep(1000);
    //ss_sleep(1000);


    //-->Board configuration (Overlap trigger not allowed and  Self trigger polary under threshold)
        
    //myv1730Board->ReadReg(0x8000,&reg);// When over-under threshold
    //reg=0x52;
    //reg=0xF4240;
    //reg|= 64;


    reg=0x10050; //unipolar negative signal under threshld
    //reg=0x10010; //unipolar positive signal upper threshold 16/10
    //reg=0x10; //used for LF

    myv1730Board->WriteReg(0x8000, reg);
    printf("self trigger enabled! Value of  polarity is :0x%x\n",reg);

    //ss_sleep(1000);

    
    //-->Pulse width for self trigger (useless with this configuration)

    //myv1730Board->ReadReg(0x1070,&reg);// program window of time
    //reg=20;//0x14
    //myv1730Board->WriteReg(0x1070, reg);
    //myv1730Board->ReadReg(0x1070,&reg);
    //printf("value of 0x1070 is : 0x%x\n",reg);

	}

  
    //-->Fron panel TRG OUT (for me useless)

    myv1730Board->ReadReg(0x8110,&reg);// Set MODE
    reg|=0xE00000FF;
    myv1730Board->ReadReg(0x8100,&reg);// Set the start
    reg|=4;
    myv1730Board->WriteReg(0x8100, reg);
    myv1730Board->ReadReg(0x8100,&reg);
  
  //printf("star reg  / value of 0x8100 is : 0x%x\n",reg);
  //myv1730Board->v1730_AcqCtl(V1730_RUN_START);

        
  //-->Acquisition status

  //myv1730Board->ReadReg(0x8104,&reg);
  //printf("Start value /  value of 0x8104 is : 0x%x\n",reg);

  //myv1730Board->ReadReg(V1730_EVENT_STORED,&reg);
  //printf("read value: %x\n",reg);

  


      myv1730Board->ReadReg(V1730_ACQUISITION_STATUS,&reg);
      cm_msg(MINFO, "AcqInit", "Acquisition Status : 0x%x", reg);
      if (!(reg & 0x80)) nInitOk++;
    } // setup != 0
  } // loop over module
  
  // Abort if PLL not locked properly
  if (nInitOk) {
    cm_msg(MINFO, "AcqInit", "PLL issues (%d)", nInitOk);
    return FE_ERR_HW;
  }
  
  // Start boards
  for (int module=0;module<N_V1730;module++) {
    if (V1730_BASE[module] < 0) continue;   // Skip unconnected board
    // Start run then wait for trigger
      myv1730Board->v1730_AcqCtl(V1730_RUN_START);
  }
 
  linRun = 1;
  
  // printf("Enter a value...\n");
  //char h;
  //scanf("%s%",&h);



  printf("End of Begin of Run\n");
  //sprintf(stastr,"GrpEn:0x%x", tsvc[0].group_mask); 
  set_equipment_status(equipment[0].name, "running", "#00FF00");    

  time_t mytime = time(NULL);
  char * time_str = ctime(&mytime);
  time_str[strlen(time_str)-1] = '\0';
  printf("Time at begin: %s\n", time_str);


  total_seconds=0;
  begin= clock();
                                                                    
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

  printf("EOR\n");

  time_t mytime = time(NULL);
  char * time_str = ctime(&mytime);
  time_str[strlen(time_str)-1] = '\0';
  printf("Time at end: %s\n", time_str);

  // Stop run 
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);

  //myv1730Board->WriteReg(0xEF28, 1);
  // myv1730Board->WriteReg(0xEF24, 1);
  // Stop DAQ for seting up the parameters
  done = 0;
  stop_req = 0;
  linRun = 0;

  end=clock();

  float seconds=(float)(end-begin)/CLOCKS_PER_SEC;

    total_seconds=total_seconds+seconds;

  std:: cout<<"Total Execution time: "<<total_seconds<<std::endl;
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  linRun = 0;
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  linRun = 1;
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{

  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  char str[128];
  static DWORD evlimit;

  if (stop_req && done==0) {
    db_set_value(hDB,0,"/logger/channels/0/Settings/Event limit", &evlimit, sizeof(evlimit), 1, TID_DWORD); 
    if (cm_transition(TR_STOP, 0, str, sizeof(str), BM_NO_WAIT, FALSE) != CM_SUCCESS) {
      cm_msg(MERROR, "feodeap", "cannot stop run: %s", str);
    }
    linRun = 0;
    done = 1;
    cm_msg(MERROR, "feodeap","feodeap Stop requested");
  }
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\
  Readout routines for different events
\********************************************************************/
int Nloop, Ncount;

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  register int i;  // , mod=-1;
  register uint32_t lam;
  DWORD reg;

  for (i = 0; i < count; i++) {

    myv1730Board->ReadReg(V1730_EVENT_STORED,&reg);
    lam = reg;
    
    if (lam) {
      Nloop = i;
      Ncount = count;
      if (!test){
	//  printf("poll successfull, V1730_EVENT_STORED: 0x%x\n", lam);
        return lam;
      }
    }
    ss_sleep(1);
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
int vf48_error = 0;
INT read_trigger_event(char *pevent, INT off)
{

  struct timeval start,stop;
  gettimeofday(&start,NULL);


  //  printf("entered read trig\n");
  uint32_t sn = SERIAL_NUMBER(pevent);
  
  // Create event header
  bk_init32(pevent);
	
  myv1730Board->FillEventBank(pevent);


  //primitive progress bar
  if (sn % 100 == 0){
    printf(".");
    gettimeofday(&stop,NULL);
    printf ("Elapsed during things_to_do...: %f\n",stop.tv_sec-start.tv_sec
	    + 0.000001*(stop.tv_usec-start.tv_usec));

  }

  return bk_size(pevent);
}



INT read_temperature(char *pevent, INT off) {

  // temporary testing.  Force a software trigger on each temperature read.
  //for(int i = 0; i < 1000; i++){
  //  usleep(50);
  //  myv1730Board->WriteReg(V1730_SW_TRIGGER,0x1); 
  // }


  bk_init32(pevent);

  DWORD *pdata;
  // int addr;
  bk_create(pevent, "TEMP", TID_DWORD, (void **)&pdata);

  // DWORD temp;
  // DWORD reg;
  /* std::cout << "Chan temperatures: ";
  for (int i=0;i<16;i++) {
    addr = V1730_CHANNEL_TEMPERATURE | (i << 8);
    myv1730Board->ReadReg(addr,&temp);
    std::cout << " " << temp;
     *pdata++ =  temp;
  }
  std::cout << std::endl;

myv1730Board->ReadReg(V1730_EVENT_STORED,&reg);
printf("read value: %x\n",reg);

  */
  bk_close(pevent,pdata);


  return bk_size(pevent);
}
