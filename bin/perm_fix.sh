#!/bin/sh

sudo chgrp dialout /dev/ttyACM* /dev/ttyUSB*
sudo chmod g+rw /dev/ttyACM* /dev/ttyUSB*
