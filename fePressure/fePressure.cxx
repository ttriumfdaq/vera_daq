//
// fePressure.cxx
//
// Frontend for Phidget HUB control of power bar
//

#include <stdio.h>
#include <signal.h> // SIGPIPE
#include <assert.h> // assert()
#include <stdlib.h> // malloc()
#include <math.h>

#include "midas.h"
#include "tmfe.h"
#include "VoltageInput.hh"

void callback(INT hDB, INT hkey, INT index, void *feptr);

class Myfe :
   public TMFeRpcHandlerInterface,
   public  TMFePeriodicHandlerInterface
{
public:
   TMFE* fMfe;
   TMFeEquipment* fEq;

   VoltageInput* vin;
   int hubPort = 5;
   int serNum = 0;
   double voltage, pressure;

   Myfe(TMFE* mfe, TMFeEquipment* eq) // ctor
   {
      fMfe = mfe;
      fEq  = eq;
   }

   ~Myfe() // dtor
   {
      if(vin) delete vin;
      fMfe->Disconnect();
   }

   void Init()
   {
      fEq->fOdbEqSettings->RI("HubPort", &hubPort, true);
      fEq->fOdbEqSettings->RI("SerNo", &serNum, true);
      fEq->fOdbEqVariables->RD("Voltage", &voltage, true);
      fEq->fOdbEqVariables->RD("Pressure", &pressure, true);
      vin = new VoltageInput(hubPort, serNum, false, true);
      if(!vin->AllGood()){
         fMfe->Msg(MERROR, "Init", "Phidget connection failed! Err: %s", vin->GetErrorCode().c_str());
         delete vin;
         exit(13);
      }
      fMfe->Msg(MINFO, "Init", "VoltageInput initialized.");
   }

   std::string HandleRpc(const char* cmd, const char* args)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
      return "OK";
   }

   void HandleBeginRun()
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run!");
      fEq->SetStatus("Running", "#00FF00");
   }

   void HandleEndRun()
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run!");
      fEq->SetStatus("Stopped", "#00FF00");
   }

   void HandlePeriodic()
   {
      voltage = vin->GetVoltage();
      pressure=pow(10.0,1.667*voltage-11.33);
      std::cout << "Voltage: " << voltage << ", Pressure: " << pressure << std::endl;

      fEq->fOdbEqVariables->WD("Voltage", voltage);
      fEq->fOdbEqVariables->WD("Pressure", pressure);
      char buf[64];
      sprintf(buf, "Pressure %.2E mbar", pressure);
      fEq->SetStatus(buf, "lightgreen");

      // printf("periodic!\n");
      //char buf[256];
      //sprintf(buf, "buffered %d (max %d), dropped %d, unknown %d, max flushed %d", gUdpPacketBufSize, fMaxBuffered, fCountDroppedPackets, fCountUnknownPackets, fMaxFlushed);
      //fEq->SetStatus(buf, "#00FF00");
      //fEq->WriteStatistics();
   }
};

static void usage()
{
   fprintf(stderr, "Usage: fePressure <name> ...\n");
   exit(1);
}

int main(int argc, char* argv[])
{
   // setbuf(stdout, NULL);
   // setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   std::string name = "";

   if (argc == 2) {
      name = argv[1];
   } else {
      usage(); // DOES NOT RETURN
   }

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect("fePressure", __FILE__);
   if (err.error) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 1;
   common->LogHistory = 1;
   //common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, "fePressure", common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   mfe->RegisterEquipment(eq);

   Myfe* myfe = new Myfe(mfe, eq);

   mfe->RegisterRpcHandler(myfe);

   //mfe->SetTransitionSequenceStart(910);
   //mfe->SetTransitionSequenceStop(90);
   //mfe->DeregisterTransitionPause();
   //mfe->DeregisterTransitionResume();

   myfe->Init();

   mfe->RegisterPeriodicHandler(eq, myfe);

   eq->SetStatus("Started...", "white");

   while (!mfe->fShutdownRequested) {
      mfe->PollMidas(10);
   }

   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
