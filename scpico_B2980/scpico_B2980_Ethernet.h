//
//  scpico_B2980_Ethernet.h
//
//
//  Created by Jérémy Chenard on 10/09/21.
//  Class B2985A_Ethernet used to connect and commands at Keysight B2985A using the Ethernet to communicate.
//
//  Usage: From/homelocal/vera-daq/vrae/online/scpico_B2980_ethernet/ directory run the executabale ./scpico_B2980_Eth


/*---------------------- LIbrairie use ----------------------*/
#ifndef SCPICO_B2980_ETHERNET_H
#define SCPICO_B2980_ETHERNET_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sstream>
#include <fstream>

#include <netinet/tcp.h>
#include <netinet/in.h>

#define _XOPEN_SOURCE_EXTENDED 1
#include <arpa/inet.h>  //inet_addr()
#include <unistd.h>     //close()

// Use to show or hide the SCPI command when sent
#define SHOW 1
#define HIDE 0

using namespace std;

// #include <math.h>
// #include <iomanip>
// #include "midas.h"
// #include "mscb.h"
// #include "mfe.h"

/*---------------------- Function declarations ----------------------*/
class B2985A_Ethernet
{
  struct sockaddr_in MyAddress;

  string My_Hostname, MyIp_address;
  int MyPort, MySocket; 
  char buffer_meas_char[4096];

  public:
    B2985A_Ethernet(const string hostname, const string ip, const int port); 
    ~B2985A_Ethernet(); 

    void initialization_B2985A();

    bool connection_B2985A_LAN();
    bool close_B2985A_LAN();

    //Communication with the B2985A.
    bool send_SCPI(const string command, const bool show_command = false);
    void read(char* buf);

    void current_settings(const int NPLC_value, const string max, const string min);
    
    void meas_single_current(double buf[]);
    void meas_buffer_current(const int num_meas, double buffer_meas_double[]);

    void measure_convert_double(char* buf_char, double buf_double[]);
    double curr_mean_measurement();

    void save_txt(string txt_path, double data[], int num_meas);
};

#endif
