//
//  scpico_new.cxx
//
//
//  Created by Giacomo Gallina on 18/11/19.
//
//
//
//  Usage: From Home/online/scpico_new directory run the executabale ./scpico_new
//  To compile it, use "make" from the same directory
// 
//  This code is mainly used for beam map. Therefore I use the autorange since the current can change of different order of magnitude in both
//  direction and there is not an easy way to do an auotrange 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "mfe.h"
//#include "mscb.h"
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>

//FOR RS232

#include "rs232.h"



/* make frontend functions callable from the C framework */


/*-- Globals -------------------------------------------------------*/


//GLOBAL VARIABLES

const std::string devId = "KEITHLEY INSTRUMENTS";
int cport_nr=16;        /* /dev/ttyUSB0 on Linux machine*/
const int min_cport_nr=16;        /* /dev/ttyUSB0 on Linux machine*/
const int max_cport_nr=21;        /* /dev/ttyUSB5 on Linux machine*/
int bdrate=9600;

char mode[]={'8','N','1',0};
char str[2][512];
const int bufsize = 4095;
unsigned char buf[4096];

//Some strange erros that sometimes happens
unsigned char asterish='*';

unsigned char small_dot='.';

char zero_string[]="-0.000.";
int old_value=0;

int busy;//To avoid to write in the same time

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "scpico_new";

/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;

/* global pointer to file */
FILE * fp2;

/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;

// extern HNDLE hDB, hDD, hSet, hControl;
extern HNDLE hDB, hSet, hDD, hControl;




#define SCPICO_SETTINGS_STR(_name) char const *_name[] = {      \
        "VOLTAGE = FLOAT : 0",                                  \
        "PLC = INT : 0",                                        \
        "VOLTAGE_SOURCE_STATUS = INT : 0",                      \
        "",                                                     \
        NULL }

typedef struct {
    float   VOLTAGE;
    int PLC;
    int VOLTAGE_SOURCE_STATUS;
} PICO_SETTINGS;


PICO_SETTINGS PICO_settings;

int lmscb_fd;
HNDLE hmyFlag;

/*-- Function declarations -----------------------------------------*/

//MIDAS FUNCTION
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

INT read_current_SCPICO(char *pevent, INT off);
INT read_SCPICO_io_event(char *pevent, INT off);

void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info);



EQUIPMENT equipment[] = {

    { "SCPICO_new",                 /* equipment name */
      {
          82,
          0,     /* event ID, trigger mask */
          "SYSTEM",              /* event buffer */
          EQ_PERIODIC ,      /* equipment type */
          0,     /* event source */
          "MIDAS",                /* format */
          TRUE,                   /* enabled */
          RO_ALWAYS,             /* read always */
          5000,                    /* read every 5 sec */
          0,                      /* stop run after this event limit */
          0,                      /* number of sub events */
          0,                      /* don't log history */
          "", "", "",
      },
      read_SCPICO_io_event,       /* readout routine */
    },
    {
        "Pico_Current",             /* equipment name */
        {
            83,
            0,            /* event ID, corrected with feIndex, trigger mask */
            "SYSTEM",               /* event buffer */
            EQ_PERIODIC,            /* equipment type */
            0,                      /* event source */
            "MIDAS",                /* format */
            TRUE,                   /* enabled */
            RO_ALWAYS |    /* read when running and on transitions */
            RO_ODB,                 /* and update ODB */
            1000,                   /* read every 1 sec */
            0,                      /* stop run after this event limit */
            0,                      /* number of sub events */
            1,                      /* log history */
            "", "", ""
        },
        read_current_SCPICO,       /* readout routine */
    },
    {""}
};

//----------------------------------------------------------------





/********************************************************************\
     Callback routines for system transitions

     These routines are called whenever a system transition like start/
     stop of a run occurs. The routines are called on the following
     occations:

     frontend_init:  When the frontend program is started. This routine
     should initialize the hardware.

     frontend_exit:  When the frontend program is shut down. Can be used
     to releas any locked resources like memory, commu-
     nications ports etc.

     begin_of_run:   When a new run is started. Clear scalers, open
     rungates, etc.

     end_of_run:     Called on a request to stop a run. Can send
     end-of-run event and close run gates.

     pause_run:      When a run is paused. Should disable trigger events.

     resume_run:     When a run is resumed. Should enable trigger events.
     \********************************************************************/


//Change Voltage Run TIme
void set_VOLTAGE(INT hDB, INT hseq, void *info){



    float voltage_to_set;
    int status, size;
    size = sizeof(voltage_to_set);
    status = db_get_value(hDB, 0,"/Equipment/SCPICO_new/Settings/VOLTAGE", &voltage_to_set, &size, TID_FLOAT, FALSE);
    (void)status;		// This is to remove the unused variable warning, we may want to use status later.


    if (abs(voltage_to_set)<10)//10
        {
            sprintf(str[1], "SOUR:VOLT:RANG 10\n");

        }else if (abs(voltage_to_set)<50)//50
        {
            sprintf(str[1], "SOUR:VOLT:RANG 50\n");

        }else if (abs(voltage_to_set)<500)
        {
            sprintf(str[1], "SOUR:VOLT:RANG 500\n");

        }else
        {
            printf("Error, invalide voltage. Must be -500< V < 500 .  \n");

        }


    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


    //Set actual voltage
    sprintf(str[1], "SOUR:VOLT %g\n",voltage_to_set);


    std::cout<<"Setting Voltage source "<<str[1]<<std::endl;
    // strcpy(str[0], "SOUR:VOLT 53\n");
    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);



}


//Change NPLC runtime
void set_NPLC(INT hDB, INT hseq, void *info){


    int PLC;
    int size = sizeof(PLC);
    db_get_value(hDB, 0,"/Equipment/SCPICO_new/Settings/PLC", &PLC, &size, TID_INT, FALSE);


    //Set actual voltage
    sprintf(str[1], "CURR:NPLC %d\n",PLC);

    std::cout<<"Setting PLC to "<<str[1]<<std::endl;

    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


}


//Turn ON/OFF voltage source runtime
void set_voltage_source_status(INT hDB, INT hseq, void *info){


    int voltage_sr_status;
    int size = sizeof(voltage_sr_status);
    db_get_value(hDB, 0,"/Equipment/SCPICO_new/Settings/VOLTAGE_SOURCE_STATUS", &voltage_sr_status, &size, TID_INT, FALSE);

    //turn off soruce
    if(voltage_sr_status==0){
      
        
        sprintf(str[1], "SOUR:VOLT:STAT OFF\n");
        
    }else if(voltage_sr_status==1){
        
        sprintf(str[1], "SOUR:VOLT:STAT ON\n");
         
    }else{
        
        std::cout<<" Error. Values Allowed [0/1]"<<std::endl;
        exit(-1);
        
    }
    
    std::cout<<"Setting Voltage source to "<<str[1]<<std::endl;

    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


}




/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{



    printf("\n\n\
               **********************************************************\n\
               *                                                        *\n\
               *                  MIDAS  PICO INTERFACE                 *\n\
               *                                                        *\n\
               *                Management of PICO stage                *\n\
               *                    for nEXO experiment                 *\n\
               *                                                        *\n\
               *                                                        *\n\
               **********************************************************\n\n");

    //MIDAS
    int status = 1;
    int size;
    char set_str[80];

    SCPICO_SETTINGS_STR(SCPICO_settings_str);

    /* hardware initialization */
    /* print message and return FE_ERR_HW if frontend should not be started */

    cm_get_experiment_database(&hDB, NULL);

    /* Map /equipment/Trigger/settings for the sequencer */
    sprintf(set_str, "/Equipment/%s/Settings", "scPICO_new");


    /* create PARAM settings record */
    status = db_create_record(hDB, 0, set_str, strcomb(SCPICO_settings_str));
    if (status != DB_SUCCESS)  return FE_ERR_ODB;
    // status = db_find_key(hDB, 0, set_str, &hSet);

    // if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);

    //Structure for adressesses
    // status = db_find_key(hDB, hSet, "DD", &hDD);

    //if (status != DB_SUCCESS)  return FE_ERR_ODB;


    printf("HOT links creation....\n");


    status = db_find_key(hDB, 0, "/Equipment/SCPICO_new/Settings/VOLTAGE", &hmyFlag);
    if (status == DB_SUCCESS) {
        /* Enable hot-link on settings/ of the equipment */
        if (hmyFlag) {
            size = sizeof(BOOL);
            if ((status = db_open_record(hDB, hmyFlag, &(PICO_settings.VOLTAGE)
                                         , size, MODE_READ
                                         , set_VOLTAGE, NULL)) != DB_SUCCESS)
                return status;
        }
    }else{
        cm_msg(MERROR, "fepico", "cannot access set_VOLTAGE");
    }



    status = db_find_key(hDB, 0, "/Equipment/SCPICO_new/Settings/PLC", &hmyFlag);
    if (status == DB_SUCCESS) {
        /* Enable hot-link on settings/ of the equipment */
        if (hmyFlag) {
            size = sizeof(BOOL);
            if ((status = db_open_record(hDB, hmyFlag, &(PICO_settings.PLC)
                                         , size, MODE_READ
                                         , set_NPLC, NULL)) != DB_SUCCESS)
                return status;
        }
    }else{
        cm_msg(MERROR, "fepico", "cannot access set_VOLTAGE");
    }

    
    
    status = db_find_key(hDB, 0, "/Equipment/SCPICO_new/Settings/VOLTAGE_SOURCE_STATUS", &hmyFlag);
    if (status == DB_SUCCESS) {
        /* Enable hot-link on settings/ of the equipment */
        if (hmyFlag) {
            size = sizeof(BOOL);
            if ((status = db_open_record(hDB, hmyFlag, &(PICO_settings.VOLTAGE_SOURCE_STATUS)
                                         , size, MODE_READ
                                         , set_voltage_source_status, NULL)) != DB_SUCCESS)
                return status;
        }
    }else{
        cm_msg(MERROR, "fepico", "cannot access VOLTAGE_SOURCE_STATUS");
    }
    



    printf("Hot link creating done !");


    printf("Opening SCPICO device using SERIAL RS 232 port ...\n");
    printf("Brate=%d\n",bdrate);

    bool connected = false;
    for(cport_nr = min_cport_nr; cport_nr <= max_cport_nr; cport_nr++){
        printf("Connection Port=%d\n",cport_nr);
        connected = (RS232_OpenComport(cport_nr, bdrate, mode) == 0);
        if(connected){
            strcpy(str[0], "*IDN?\n");
            RS232_cputs(cport_nr, str[0]);
            ss_sleep(1000);
            int n = RS232_PollComport(cport_nr, buf, bufsize);
            std::cout << "Received " << n << "characters, : " << buf << std::endl;
            std::string str = (char*)buf;
            if(str.find(devId) == std::string::npos){
                RS232_CloseComport(cport_nr);
                connected = false;
            } else {
                std::cout << "Success" << std::endl;
                break;
            }
        }
    }

    if(!connected){
        printf("Cannot open SCPICO, check connection or device!\n");
        return(0);
    }


    printf("Reset the unit.....\n\n");
    strcpy(str[0], "*RST\n");
    RS232_cputs(cport_nr, str[0]);
    ss_sleep(1000);


    printf("Auto correction OFF.....\n\n");
    strcpy(str[0], "SYST:ZCH OFF\n");
    RS232_cputs(cport_nr, str[0]);
    ss_sleep(1000);

    //Set the voltage to the predefined value of zero

    float voltage_to_set=0;
    size = sizeof(voltage_to_set);
    db_set_value(hDB,0,"/Equipment/SCPICO_new/Settings/VOLTAGE", &voltage_to_set, sizeof(voltage_to_set), 1, TID_FLOAT);


    //Get the value for the ODB and enable the source
    status = db_get_value(hDB, 0,"/Equipment/SCPICO_new/Settings/VOLTAGE", &voltage_to_set, &size, TID_FLOAT, FALSE);


    if (abs(voltage_to_set)<10)//10
        {
            sprintf(str[1], "SOUR:VOLT:RANG 10\n");

        }else if (abs(voltage_to_set)<50)//50
        {
            sprintf(str[1], "SOUR:VOLT:RANG 50\n");

        }else if (abs(voltage_to_set)<500)
        {
            sprintf(str[1], "SOUR:VOLT:RANG 500\n");

        }else
        {
            printf("Error, invalide voltage. Must be -500< V < 500 .  \n");
            return 0;
        }


    // std::cout<<"Setting Voltage source range "<<str[1]<<std::endl;
    // strcpy(str[0], "SOUR:VOLT 53\n");
    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);



    sprintf(str[1], "SOUR:VOLT %g\n",voltage_to_set);
    std::cout<<"Setting Voltage source "<<str[1]<<std::endl;
    // strcpy(str[0], "SOUR:VOLT 53\n");
    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


    printf("Voltage surce OFF.....\n\n");
    //Turn OFF voltage source as default
    int src_status=0;
    db_set_value(hDB,0,"/Equipment/SCPICO_new/Settings/VOLTAGE_SOURCE_STATUS", &src_status, sizeof(src_status), 1, TID_INT);


    //Should be already done in previous command
    //strcpy(str[0], "SOUR:VOLT:STAT ON\n");
    //RS232_cputs(cport_nr, str[0]);
    //ss_sleep(1000);

    printf("Enabling Auto Range.....\n\n");
    strcpy(str[0], "CURR:RANG:AUTO ON\n");
    RS232_cputs(cport_nr, str[0]);
    ss_sleep(1000);

    //printf("Setting range to 2e-6.....\n\n");
    //strcpy(str[0], "CURR:RANG 2e -6\n");
    //RS232_cputs(cport_nr, str[0]);
    //ss_sleep(1000);



    //Set PLC to predefined value
    int PLC=10;
    size = sizeof(PLC);
    db_set_value(hDB,0,"/Equipment/SCPICO_new/Settings/PLC", &PLC, sizeof(PLC), 1, TID_INT);


    //Get the value for the ODB and enable the source
    status = db_get_value(hDB, 0,"/Equipment/SCPICO_new/Settings/PLC", &PLC, &size, TID_INT, FALSE);

    sprintf(str[1], "CURR:NPLC %d\n",PLC);
    std::cout<<"Setting PLC to "<<str[1]<<std::endl;
    // strcpy(str[0], "SOUR:VOLT 53\n");
    RS232_cputs(cport_nr, str[1]);
    ss_sleep(1000);


    printf("Unit ready.....!\n\n");


    return FE_SUCCESS;

}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{

    printf("Closing...\n");



    printf("Voltage surce OFF.....\n\n");
    strcpy(str[0], "SOUR:VOLT:STAT OFF\n");
    RS232_cputs(cport_nr, str[0]);
    ss_sleep(1000);




    //Closing serial PORT
    RS232_CloseComport(cport_nr);

    return SUCCESS;

}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

    return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
    return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
    /* if frontend_call_loop is true, this routine gets called when
       the frontend is idle or once between every event */
    ss_sleep(100);
    return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events


/*-- Trigger event routines ----------------------------------------*/


INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
    int i;
    DWORD lam;

    for (i = 0; i < count; i++) {
        lam = 1;
        if (lam & LAM_SOURCE_STATION(source))
            if (!test)
                return lam;
    }
    return 0;
}



/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
    switch (cmd) {
    case CMD_INTERRUPT_ENABLE:
        break;
    case CMD_INTERRUPT_DISABLE:
        break;
    case CMD_INTERRUPT_ATTACH:
        break;
    case CMD_INTERRUPT_DETACH:
        break;
    }
    return SUCCESS;
}

/*-- event --------------------------------------------------*/


INT read_SCPICO_io_event(char *pevent, INT off)
{
    float *pfdata;
    /* init bank structure */
    bk_init(pevent);

    /* create Phidget bank */
    bk_create(pevent, "CURR", TID_FLOAT, (void **) &pfdata);


    bk_close(pevent, pfdata);


    return 0;

    printf("event size %d\n", bk_size(pevent));

    return bk_size(pevent);
}




//ROUTINE TO MANAGE THE READING OF the current


INT read_current_SCPICO(char *pevent, INT off) {


    int i,n=0;
    double *pdata;
    float temp;

    while(busy==1){

        //Do nothing
        printf("Wasting time...\n");

    }

    //Set as not free
    busy=1;


    bk_init32(pevent);


    bk_create(pevent, "CRTT", TID_DOUBLE, (void **)&pdata);



    strcpy(str[0], "READ?\n");


    //SEND COMMAND
    RS232_cputs(cport_nr, str[0]);


    while(1)
        {

            //Wait before to receive the answer
            ss_sleep(1000);
            //RICEVO RISPOSTA
            n = RS232_PollComport(cport_nr, buf, bufsize);


            if(n > 0)
                {
                    buf[n] = 0;

                    for(i=0; i < n; i++)
                        {
                            if(buf[i] < 32)
                                {
                                    buf[i] = '.';
                                }
                        }


                    //Avoid errors
                    if(1){

                        //NO error happens

                        //temp=atof((char*)buf);

                        //temp=(char*)buf;

                        printf("Received %i bytes, Read: %s\n", n, (char *)buf);
                        //printf("pluto=%f\n",temp);



                        if((atof((char*)buf))!=0){
                            break;
                        }


                    }else{
                        //An error happens
                        printf("An error happens....Ship line...\n");
                        break;
                    }
                }

            ss_sleep(1000);

        }


    //Find the A of AMPERE in the Answer, obtain a pointer to that memory adress and put in it a zero
    char *c = strchr((char*)buf, 'A');
    if (c != NULL){
        *c = 0;
    }
    else{

        return 0;
    }

    temp=atof((char*)buf);

    //clock_t start;
    //double cpu_time_used;
    //start = (double) clock();

    time_t mytime = time(NULL);
    char * time_str = ctime(&mytime);
    time_str[strlen(time_str)-1] = '\0';
    printf("Current Time : %s\n", time_str);

    //Writing on existing file
    fp2 = fopen ("/home/exo/online/scpico_new/Time_vs_I.txt", "a");
    fprintf(fp2,"%s\t%e\n",time_str,temp);
    fclose(fp2);


    printf("Current wrote in MIDAS: %e\n\n",temp);


    *pdata++ =  temp;

    bk_close(pevent,pdata);

    //Now it is free

    busy=0;


    return bk_size(pevent);
}
