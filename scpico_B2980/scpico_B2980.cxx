//
//  scpico_B2980.cxx
//
//
//  Created by Lars Martin on 18/11/19, based on scpico_new.cxx.
//  the Keysight B2980 family connects to USB as a usbtmc device, which is just an open file socket accepting SCPI communication directly, no setup needed.
//
//  Modified by Giacomo Gallina to accept commands and plot in history
//
//
//  Usage: From Home/online/scpico_B2980 directory run the executabale ./scpico_B2980
//  To compile it, use "make" from the same directory


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
// #include "mscb.h"
#include "mfe.h"
#include <unistd.h>
#include <math.h>
#include <time.h>
//#include <stdio.h>
//#include <string.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>

#include "scpico_B2980_Ethernet.h"

using namespace std;


/*-- Globals -------------------------------------------------------*/

// ---------------- New ----------------
// Connection configuration
const string hostname   = "K-B2985A-20983";
//const string ip_address = "169.254.5.2";
const string ip_address = "142.90.106.238"; 
const int port_nb       = 5025;

// Current settings
const string curr_max = "2e-3";
const string curr_min = "2e-12";

// Creation of the object scpico
B2985A_Ethernet scpico(hostname, ip_address, port_nb);

/*
//int NPLC_value = 100; //range: 10us - 100PLC(60Hz), PLC = nplc/power line frequency, NPLC = time * Power line frequency, PLF =
int sleep_time = 3; // in seconds, ideally 2*NPLC, but try other ranges NPLC * 1/60 = integration time (s)
int buf_size = 4095;

std::fstream device;
char devname[] = "/dev/usbtmc";
const std::string devId = "B298";
*/
// -------------------------------------


std::string str;
int n;
char buf[4096];


/* The frontend name (client name) as seen by other MIDAS clients   */
char const *frontend_name = "scpico_B2980";

/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;

/* global pointer to file */
FILE * fp2;

/* do not read Common from ODB, overwrite it, TRUE is standard behaviour */
BOOL equipment_common_overwrite = TRUE;

// HNDLE hDB, hDD, hSet, hControl;




#define SCPICOB290_SETTINGS_STR(_name) char const *_name[] = {  \
    "PLC = INT : 0",\
    "VOLTAGE = FLOAT : 0",\
    "VOLTAGE_STATUS = INT : 0",\
    "",                                                         \
    NULL }

typedef struct {
  int   PLC;
  float   VOLTAGE;
  int   VOLTAGE_STATUS;
} PICOB290_SETTINGS;


PICOB290_SETTINGS PICOB290_settings;

int lmscb_fd;
HNDLE hmyFlag;

/*-- Function declarations -----------------------------------------*/

//MIDAS FUNCTION
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

INT read_current_SCPICO(char *pevent, INT off);
INT read_SCPICO_io_event(char *pevent, INT off);

void param_callback(INT hDB, INT hKey, void *info);
void register_cnaf_callback(int debug);
void seq_callback(INT hDB, INT hseq, void *info);
void seq_HOLD_FINAL_TEMPERATURE(INT hDB, INT hseq, void *info);


EQUIPMENT equipment[] = {

  { "SCPICO_B2980",                 /* equipment name */
    {
      84,
      0,     /* event ID, trigger mask */
      "SYSTEM",              /* event buffer */
      EQ_PERIODIC ,      /* equipment type */
      0,     /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS,             /* read always */
      1000,                    /* read every 5 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",
    },
    read_SCPICO_io_event,       /* readout routine */
  },
  {
    "Pico_B2980_Current",             /* equipment name */
    {
      85,
      0,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_current_SCPICO,       /* readout routine */
  },
  {""}
};

//----------------------------------------------------------------




/********************************************************************\
     Callback routines for system transitions

     These routines are called whenever a system transition like start/
     stop of a run occurs. The routines are called on the following
     occations:

     frontend_init:  When the frontend program is started. This routine
     should initialize the hardware.

     frontend_exit:  When the frontend program is shut down. Can be used
     to releas any locked resources like memory, commu-
     nications ports etc.

     begin_of_run:   When a new run is started. Clear scalers, open
     rungates, etc.

     end_of_run:     Called on a request to stop a run. Can send
     end-of-run event and close run gates.

     pause_run:      When a run is paused. Should disable trigger events.

     resume_run:     When a run is resumed. Should enable trigger events.
     \********************************************************************/



//Change NPLC runtime

void set_NPLC(INT hDB, INT hseq, void *info){


       int PLC;
       int size;
       size = sizeof(PLC);
       db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/PLC", &PLC, &size, TID_INT, FALSE);


       std::cout<<" Set PLC to "<<PLC<<std::endl;

// ---------------- New ----------------
      /*
      str = ":SENS:CURR:NPLC " + std::to_string(PLC);
      device << str << std::endl;
      */
      scpico.send_SCPI(":SENS:CURR:NPLC " + to_string(PLC) + "\n");
// -------------------------------------



}


//Set Voltage value

void set_voltage_value(INT hDB, INT hseq, void *info){

    float VOLTAGE;
    int size;
    size = sizeof(VOLTAGE);
    db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE", &VOLTAGE, &size, TID_FLOAT, FALSE);

    std::cout<<" Set VOLTAGE to "<<VOLTAGE<<std::endl;

// ---------------- New ----------------
    /*
    str = ":SOUR:VOLT:LEV " + std::to_string(VOLTAGE);
    device << str << std::endl;
    */

        float RANGE;

	if(VOLTAGE<=-19){

	   RANGE=-1000;

	   scpico.send_SCPI(":SOUR:VOLT:RANG " + to_string(RANGE) + "\n");

	}else if(VOLTAGE<20){

	  RANGE=20;

	  scpico.send_SCPI(":SOUR:VOLT:RANG " + to_string(RANGE) + "\n");

	}else{

	  RANGE=1000;

	  scpico.send_SCPI(":SOUR:VOLT:RANG " + to_string(RANGE) + "\n");

	}


	//Now set the voltage
	
    scpico.send_SCPI(":SOUR:VOLT:LEV " + to_string(VOLTAGE) + "\n");
// -------------------------------------

}

//Enable Voltage value
void enable_voltage_source(INT hDB, INT hseq, void *info){


    int VOLTAGE_STATUS;
    int size;
    size = sizeof(VOLTAGE_STATUS);
    db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE_STATUS", &VOLTAGE_STATUS, &size, TID_INT, FALSE);

    std::cout<<" Set VOLTAGE to "<<VOLTAGE_STATUS<<std::endl;

// ---------------- New ----------------
    /*
    str = ":OUTP:STAT " + std::to_string(VOLTAGE_STATUS);
    device << str << std::endl;
    */

    scpico.send_SCPI(":OUTP:STAT " + to_string(VOLTAGE_STATUS) + "\n");
// -------------------------------------
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{



  printf("\n\n\
               **********************************************************\n\
               *                                                        *\n\
               *                  MIDAS B290 INTERFACE                  *\n\
               *                                                        *\n\
               *                Management of PICO stage                *\n\
               *                    for nEXO experiment                 *\n\
               *                                                        *\n\
               *                                                        *\n\
               **********************************************************\n\n");


  //MIDAS
  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */



  //MIDAS
  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */

  SCPICOB290_SETTINGS_STR(SCPICOB290_settings_str);

  cm_get_experiment_database(&hDB, NULL);


  // /* Map /equipment/Trigger/settings for the sequencer */
  char set_str[80];
  sprintf(set_str, "/Equipment/%s/Settings", "SCPICO_B2980");

  int status, size;
  // /* create PARAM settings record */
  status = db_create_record(hDB, 0, set_str, strcomb(SCPICOB290_settings_str));
  if (status != DB_SUCCESS)  return FE_ERR_ODB;
  // status = db_find_key(hDB, 0, set_str, &hSet);


  //PLC settings
  status = db_find_key(hDB, 0, "/Equipment/SCPICO_B2980/Settings/PLC", &hmyFlag);
  if (status == DB_SUCCESS) {
      /* Enable hot-link on settings/ of the equipment */
      if (hmyFlag) {
          size = sizeof(BOOL);
          if ((status = db_open_record(hDB, hmyFlag, &(PICOB290_settings.PLC)
                                        , size, MODE_READ
                                        , set_NPLC, NULL)) != DB_SUCCESS)
              return status;
      }
  }else{
      cm_msg(MERROR, "fepico", "cannot access set PLC");
  }




  //VOLTAGE SETTINGS
  status = db_find_key(hDB, 0, "/Equipment/SCPICO_B2980/Settings/VOLTAGE", &hmyFlag);
  if (status == DB_SUCCESS) {
      /* Enable hot-link on settings/ of the equipment */
      if (hmyFlag) {
          size = sizeof(BOOL);
          if ((status = db_open_record(hDB, hmyFlag, &(PICOB290_settings.VOLTAGE)
                                        , size, MODE_READ
                                        , set_voltage_value, NULL)) != DB_SUCCESS)
              return status;
      }
  }else{
      cm_msg(MERROR, "fepico", "cannot access set_VOLTAGE");
  }




  //SOURCE STATUS
  status = db_find_key(hDB, 0, "/Equipment/SCPICO_B2980/Settings/VOLTAGE_STATUS", &hmyFlag);
  if (status == DB_SUCCESS) {
      /* Enable hot-link on settings/ of the equipment */
      if (hmyFlag) {
          size = sizeof(BOOL);
          if ((status = db_open_record(hDB, hmyFlag, &(PICOB290_settings.VOLTAGE_STATUS)
                                        , size, MODE_READ
                                        , enable_voltage_source, NULL)) != DB_SUCCESS)
              return status;
      }
  }else{
      cm_msg(MERROR, "fepico", "cannot access set VOLTAGE STATUS");
  }



   printf("Hot link creating done !");

// ---------------- New ----------------
  scpico.initialization_B2985A();

  // Try connecting to the B2985A
  if(! scpico.connection_B2985A_LAN())
  {
    cm_msg(MERROR, "fepico", "Failed to connect to device.");

    return 0;
  }
/* CHange for Ethernet
    printf("Opening SCPICO device using USBTMC device ...\n");
    for(int devnum = 0; devnum < 16; devnum++){
        char dn[128];
        sprintf(dn, "%s%d", devname, devnum);
        printf("Trying Device=%s\n",dn);

        device.open(dn);
        if(device.is_open()){
            device << "*IDN?" << std::endl;
            sleep(sleep_time);

            device.getline(buf, buf_size);
            n = device.gcount();
            std::cout << "Received " << n << " characters, : " << buf << std::endl;
            if(std::string(buf).find(devId) == std::string::npos){
                device.close();
            } else {
                std::cout << "Success" << std::endl;
                break;
            }
        }
    }
    if(! device.is_open()){
        cm_msg(MERROR, "fepico", "Failed to connect to device.");
        return 0;
    }


  printf("Resetting device !");

  // Clear status
  str = "*CLS\r";
  device << str << std::endl;
  str = "*RST\r";
  device << str << std::endl;



  // format output to have voltmeter, current, and source
  //str = ":FORM:ELEM:SENS VOLT,CURR,SOUR\r";
  str = ":FORM:ELEM:SENS TIME,CURR\r";  // adding time stamp?
  device << str << std::endl;

  // Try setting NPLC etc
  //str = ":SENS:CURR:NPLC " + std::to_string(NPLC_value);
  //device << str << std::endl;

*/
// -------------------------------------

  //Set PLC to predefined value
  int PLC=10;
  size = sizeof(PLC);
  db_set_value(hDB,0,"/Equipment/SCPICO_B2980/Settings/PLC", &PLC, sizeof(PLC), 1, TID_INT);

  //Get the value from the ODB just saved and set it.
  status = db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/PLC", &PLC, &size, TID_INT, FALSE);

  std::cout<<"Initial setting"<<PLC<<std::endl;
  
  // ---------------- New ----------------
  // Initialise current settings
  scpico.current_settings(PLC, curr_max, curr_min);
/*
  str = ":SENS:CURR:NPLC " + std::to_string(PLC);
  device << str << std::endl;

  str = ":INP:STAT 1";
  device << str << std::endl;

  str = ":SENS:CURR:RANG:AUTO:LLIM 2e-12";
  device << str << std::endl;

  //Always autorange
  str = ":SENS:CURR:RANG:AUTO:ULIM 2e-3";
  device << str << std::endl;
*/
// -------------------------------------


  //Safety things for ODB set voltage source to 0 and OFF

  //Set PLC to predefined value
  float VOLTAGE=0;
  size = sizeof(VOLTAGE);
  db_set_value(hDB,0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE", &VOLTAGE, sizeof(VOLTAGE), 1, TID_FLOAT);

  //Get the value from the ODB just saved and set it.

  status = db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE", &VOLTAGE, &size, TID_FLOAT, FALSE);
  std::cout<<"Initial VOLTAGE "<<VOLTAGE<<std::endl;



  //Set PLC to predefined value
  int VOLTAGE_STATUS=0;
  size = sizeof(VOLTAGE_STATUS);
  db_set_value(hDB,0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE_STATUS", &VOLTAGE_STATUS, sizeof(VOLTAGE_STATUS), 1, TID_INT);

  //Get the value from the ODB just saved and set it.
  status = db_get_value(hDB, 0,"/Equipment/SCPICO_B2980/Settings/VOLTAGE_STATUS", &VOLTAGE_STATUS, &size, TID_INT, FALSE);
  std::cout<<"Initial VOLTAGE STATUS "<<VOLTAGE_STATUS<<std::endl;


  printf("Unit ready.....!\n\n");



  return FE_SUCCESS;

}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
// ---------------- New ----------------
/*
  str = ":INP:STAT 0";
  device << str << std::endl;
*/
  scpico.send_SCPI(":INP OFF \n", SHOW);
// -------------------------------------

  return SUCCESS;

}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events


/*-- Trigger event routines ----------------------------------------*/


INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;

  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
        return lam;
  }
  return 0;
}



/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- event --------------------------------------------------*/


INT read_SCPICO_io_event(char *pevent, INT off)
{
  float *pfdata;
  /* init bank structure */
  bk_init(pevent);

  /* create Phidget bank */
  bk_create(pevent, "CURR", TID_FLOAT, (void **) &pfdata);


  bk_close(pevent, pfdata);


  return 0;

  printf("event size %d\n", bk_size(pevent));

  return bk_size(pevent);
}

//ROUTINE TO MANAGE THE READING OF the current


INT read_current_SCPICO(char *pevent, INT off) {


  double *pdata;
// ---------------- New ----------------
  double temp[2];
// -------------------------------------

  // while(busy==1){

  //Do nothing
  //  printf("Wasting time...\n");

  //}

  bk_init32(pevent);


  bk_create(pevent, "CRTT", TID_DOUBLE, (void **)&pdata);

// ---------------- New ----------------
  // Take a single measure
  scpico.meas_single_current(temp);
/*
  str = ":MEAS?";
  device << str << std::endl;
  sleep(sleep_time);
  //sleep(1);

  device.getline(buf, buf_size);
  n = device.gcount();
  std::cout << "Received " << n << " characters, CURR I , SOUR V: " << buf << std::endl;

  temp=atof((char*)buf);

  printf("Current wrote in MIDAS: %e\n\n",temp);
*/
// -------------------------------------
  printf("Current wrote in MIDAS: %e\n\n",temp[0]);

  /*
    time_t mytime = time(NULL);
    char * time_str = ctime(&mytime);
    time_str[strlen(time_str)-1] = '\0';
    printf("Current Time : %s\n", time_str);

    //Writing on existing file
    fp2 = fopen ("/home/exo/online/scpico_B2980/Time_vs_I.txt", "a");
    fprintf(fp2,"%s\t%e\n",time_str,temp);
    fclose(fp2);



    printf("Current wrote in MIDAS: %e\n\n",temp);

  */

// ---------------- New ----------------
  //*pdata++ =  temp;
  *pdata++ = temp[0];
// -------------------------------------

  bk_close(pevent,pdata);

  return bk_size(pevent);

}
