//
//   scpico_B2980_Ethernet.cpp
//
//
//  Created by Jérémy Chenard on 10/09/21.
//  Déclaration of the function of the class B2985A_Ethernet
//
//  Usage: From/homelocal/vera-daq/vrae/online/scpico_B2980_ethernet/ directory run the executabale ./scpico_B2980_Eth

#include "scpico_B2980_Ethernet.h"

#define SUCCESS 1;
#define FAIL    0;

using namespace std;

B2985A_Ethernet::B2985A_Ethernet(const string hostname, const string ip, const int port)
{
  // Global declaration
  My_Hostname  = hostname.c_str();
  MyIp_address = ip.c_str();
  MyPort       = port;

  MySocket = -1;

  cout << "B2985A object created" << endl;
}

void B2985A_Ethernet::initialization_B2985A()
{   
  // Initialize the whole structure to zero
  memset(&MyAddress, 0, sizeof(struct sockaddr_in));

  // Set the individual fields
  MyAddress.sin_family=PF_INET; // IPv4
  MyAddress.sin_port=htons(MyPort); // Port number
  MyAddress.sin_addr.s_addr=inet_addr(MyIp_address.c_str()); // IP Address
}

B2985A_Ethernet::~B2985A_Ethernet()
{
    close_B2985A_LAN();
} 

bool B2985A_Ethernet::connection_B2985A_LAN()
{ 
  char temp_buffer[200];

  if((MySocket=socket(PF_INET, SOCK_STREAM, 0))==-1)
  {
    // Error handling 
    cout << "Error while opening the socket, errno " << errno << ": ";
    cout << strerror(errno) << endl;

    return FAIL;
  } 

  // Establish TCP connection
  if(connect(MySocket, (struct sockaddr*)&MyAddress, sizeof(struct sockaddr_in))==-1)
  {
    cout << "Failed to connect to B2985A!" << endl;
    return FAIL;
  }

  cout << "Connected to B2985A!" << endl;
  send_SCPI("*IDN?\n", SHOW);
  read(temp_buffer);
  cout << "Instrument ID: " << temp_buffer << endl;

  cout << "Reset the unit...." << endl;
  send_SCPI("*CLS\n", SHOW);
  send_SCPI("*RST\n", SHOW);

  return SUCCESS;
}

bool B2985A_Ethernet::close_B2985A_LAN()
{
  // Close socket
  send_SCPI(":INP OFF \n", SHOW);

  if(close(MySocket)==-1)
  {
    // Error handling 
    cout << "Error while closing the socket, errno " << errno << ": ";
    cout << strerror(errno) << endl;

    return FAIL;
  } 

  return SUCCESS;
}

// Communication with the B2985A.
bool B2985A_Ethernet::send_SCPI(const string command, const bool show_command)
{
  // Send SCPI command
  if(send(MySocket, command.c_str(), strlen(command.c_str()), 0) == -1) 
  {
    // Error handling 
    cout << "SCPI command failed, errno " << errno << ": ";
    cout << strerror(errno) << endl;

    return FAIL;
  } 

  if (show_command == 1)
  {
    cout << "Command sent " << command;
  }

  return SUCCESS;
}

// Read response
void B2985A_Ethernet::read(char* buf)
{
  int actual;

  if((actual = recv(MySocket, buf, 4096, 0)) == -1)
  {
    // Error handling 
    cout << "Error while reading, errno " << errno << ": ";
    cout << strerror(errno) << endl;
  }
  
  // Add zero character (C string) 
  buf[actual]=0;  
}

void B2985A_Ethernet::current_settings(const int NPLC_value, const string max, const string min)
{
  // Initialise settings
  cout << "Enabling Auto Range....." << endl;

  send_SCPI(":SENS:CURR:RANG:AUTO: ON");
  send_SCPI(":SENS:CURR:RANG:AUTO:LLIM " + min + "\n", SHOW);
  send_SCPI(":SENS:CURR:RANG:AUTO:ULIM " + max + "\n", SHOW);

  // Setting measurement speed
  send_SCPI(":SENS:CURR:NPLC " + to_string(NPLC_value) + "\n", SHOW);

  // Enabling the Current/Charge measurement
  send_SCPI(":INP ON \n", SHOW);

  cout << "Unit ready to measure current.....!" << endl << endl; 
}

void B2985A_Ethernet::meas_single_current(double buf[])
{
  send_SCPI(":SENS:FUNC CURR\n", HIDE);
  send_SCPI(":FORM:ELEM:SENS CURR\n", HIDE);
  send_SCPI(":MEAS?\n", HIDE);
  
  read(buffer_meas_char);

  measure_convert_double(buffer_meas_char, buf);
}

void B2985A_Ethernet::meas_buffer_current(const int num_meas, double buffer_meas_double[])
{
  // Declaration of temporary variable
  char temp_buf[200];

  // Setting up the trace buffer before taking measurements
  send_SCPI(":TRAC:CLE\n", SHOW);  // Clears trace buffer
  send_SCPI(":TRAC:POIN " + to_string(num_meas) + "\n", SHOW); // Sets buffer size
  send_SCPI(":TRAC:FEED:CONT NEV\n", SHOW);
  send_SCPI(":TRAC:FEED SENS\n", SHOW); // Specifies data to feed
  send_SCPI(":TRAC:FEED:CONT NEXT\n", SHOW); // Enables write buffer 
  send_SCPI(":TRAC:TST:FORM DELT\n", SHOW); // Timestamp data format delta (DELT) or absolute (ABS) 

  cout << endl;

  send_SCPI(":SENS:FUNC CURR\n", SHOW);
  send_SCPI(":FORM:ELEM:SENS CURR\n", SHOW);  // Specify the data to collect
  
  cout << "Starting measurements..." << endl;
  
  // Wait until the buffer is full
  while (strcmp(temp_buf,"NEV\n") != 0)
  {
    send_SCPI(":MEAS?\n", HIDE); // Take a measurement and store the result in the buffer
    read(temp_buf); // Empty the buffer and overwrite old string

    send_SCPI(":TRAC:POIN:ACT?\n", HIDE); // Progre
    read(temp_buf); // Empty the buffer and overwrite old string

    // Test for taking out the return of the answer
    cout << "Number of data taken : (" << strtok(temp_buf, "\n") << "/" << num_meas << ")" << endl;   
    
    send_SCPI(":TRAC:FEED:CONT?\n", HIDE); // Buffer full ?
    read(temp_buf);
  }

  cout << "The buffer is full!" << endl << endl;; 

  // Reads all data
  send_SCPI(":TRAC:DATA?\n", SHOW); 
  read(buffer_meas_char);

  cout << "Measurements in the buffer..." << endl;
  cout << buffer_meas_char << endl;  

  measure_convert_double(buffer_meas_char, buffer_meas_double);
}

void B2985A_Ethernet::measure_convert_double(char* buf_char, double buf_double[])
{
  char* pt;
  int   i = 0;

  // Split char* at every ','
  pt = strtok(buf_char, ",");

  // Separate mean measurement
  while(pt != NULL)
  {
      // Register measurement in a arry of double
      buf_double[i] = stod(pt);
      
      // Find the next measurement
      pt = strtok(NULL, ",");
      i++;
  }
}

double B2985A_Ethernet::curr_mean_measurement()
{   
  char* buffer[4];
  char* pt;
  int   i = 0;

  // Reads Mean value, return 4 datas (CHARGE, CURRENT, VOLTAGE, RESISTANCE)
  send_SCPI(":TRAC:STAT:FORM MEAN\n", SHOW); 
  send_SCPI(":TRAC:STAT:DATA?\n", SHOW); 

  // Reads statistical data  
  read(buffer_meas_char);  
  
  // Split char* at every ','
  pt = strtok(buffer_meas_char, ",");
  
  // Separate mean measurement
  while(pt != NULL)
  {
      // Register mean for the CHARGE, CURRENT, VOLTAGE, RESISTANCE in an array
      buffer[i] = pt;
      
      // Find the next measurement
      pt = strtok(NULL, ",");
      i++;
  }

  //Return only the current in double
  return stod(buffer[1]);
}

void B2985A_Ethernet::save_txt(string txt_path, double data[], int num_meas)
{
  // Pointer to file 
  FILE * fp2;

  // Find current time
  time_t mytime = time(NULL);
  char * time_str = ctime(&mytime);
  time_str[strlen(time_str)-1] = '\0';
  printf("Current Time : %s\n", time_str);

  // Decompose the date and the time
  struct tm* actual_time;
  actual_time = localtime(&mytime);

  // Creating the file name
  string txt_name = "/" +  to_string(actual_time->tm_mday) + to_string(actual_time->tm_mon) + to_string(actual_time->tm_year+1900) + "_";
  txt_name +=  to_string(actual_time->tm_hour) + to_string(actual_time->tm_min) + to_string(actual_time->tm_sec) + "_CurrentMeasures.txt";
  
  cout << "File path : " << txt_path << endl;

  //Create a different file
  cout << "File name : " << txt_name << endl;

  // Create the full file path
  txt_path += txt_name;

  // Open and write in the data from the buffer in a txt file
  fp2 = fopen (txt_path.c_str(), "a");
  fprintf(fp2,"Date: %s\nData:\n", time_str);
  
  for(int i = 0; i < num_meas; i++)
  {
    fprintf(fp2, "%e\n", data[i]);
  }

  fclose(fp2);
}

// void B2985A_Ethernet::set_plc(int plc)
// {
//   MyPlc = plc;

//   // Setting measurement speed
//   send_SCPI(":SENS:CURR:NPLC " + to_string(MyPlc) + "\n", SHOW);

//   // Calculate number of measurements taken in 1 seconde
//   num_measurement = int(MyPlc / 60);
// }

// int B2985A_Ethernet::get_NumMeas()
// {
//   return num_measurement;
// }
