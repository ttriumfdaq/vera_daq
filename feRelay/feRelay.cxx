//
// feRelay.cxx
//
// Frontend for Phidget 4x Relay
//

#include <stdio.h>
#include <signal.h> // SIGPIPE
#include <assert.h> // assert()
#include <stdlib.h> // malloc()

#include "midas.h"
#include "tmfe.h"
#include "RelayArray.hh"

void callback(INT hDB, INT hkey, INT index, void *feptr);

class Myfe :
   public TMFeRpcHandlerInterface,
   public  TMFePeriodicHandlerInterface
{
public:
   TMFE* fMfe;
   TMFeEquipment* fEq;

   RelayArray* relay;
   const unsigned int num_relays = 4;
   std::vector<bool> relaySet = std::vector<bool>(num_relays, false);
   int hubPort = 5;
   int serNum = 0;

   Myfe(TMFE* mfe, TMFeEquipment* eq) // ctor
   {
      fMfe = mfe;
      fEq  = eq;
   }

   ~Myfe() // dtor
   {
      if(relay) delete relay;
      fMfe->Disconnect();
   }

   void Init()
   {
      fEq->fOdbEqSettings->RI("HubPort", &hubPort, true);
      fEq->fOdbEqSettings->RI("SerNo", &serNum, true);
      fEq->fOdbEqSettings->RBA("Relay", &relaySet, true, num_relays);

      relaySet.clear();         // workaround for MIDAS bug in midasodb.cxx
      fEq->fOdbEqSettings->RBA("Relay", &relaySet);

      relay = new RelayArray(num_relays, hubPort, serNum, true);
      if(!relay->AllGood()){
         fMfe->Msg(MERROR, "Init", "Phidget connection failed! Err: %s", relay->GetErrorCode().c_str());
         delete relay;
         exit(13);
      }

      relaySet = GetRelayStates();
      fEq->fOdbEqSettings->WBA("Relay", relaySet);

      char tmpbuf[80];
      sprintf(tmpbuf, "/Equipment/%s/Settings/Relay", fMfe->fFrontendName.c_str());
      HNDLE hkey;
      db_find_key(fMfe->fDB, 0, tmpbuf, &hkey);
      db_watch(fMfe->fDB, hkey, callback, (void*)this);
   }

   /** \brief Function called on ODB setting change.*/
   void fecallback(HNDLE hDB, HNDLE hkey, INT index)
   {
      busy = true;
      relaySet.clear();         // workaround for MIDAS bug in midasodb.cxx
      fEq->fOdbEqSettings->RBA("Relay", &relaySet);

      if(relaySet[index])
         fMfe->Msg(MINFO, "SwitchRelay", "ON: %s", relaySet[index]?"ON":"OFF");
      else
         fMfe->Msg(MINFO, "SwitchRelay", "OFF: %s", relaySet[index]?"ON":"OFF");

      if(!SwitchRelay(index, relaySet[index])){
         fMfe->Msg(MERROR, "SwitchRelay", "Couldn't switch relay!");
      }
      fEq->SetStatus(StatString().c_str(), "lightgreen");
      busy = false;
   }

   std::string HandleRpc(const char* cmd, const char* args)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
      return "OK";
   }

   void HandleBeginRun()
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run!");
      // fEq->SetStatus("Running", "#00FF00");
   }

   void HandleEndRun()
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run!");
      // fEq->SetStatus("Stopped", "#00FF00");
   }

   void HandlePeriodic()
   {
      // printf("periodic!\n");

      if(!busy){
         std::vector<bool> relayRB = GetRelayStates();
         if(relayRB != relaySet){
            fMfe->Msg(MERROR, "HandlePeriodic", "Read back relay stats don't match settings!");
         }
         fEq->SetStatus(StatString().c_str(), "lightgreen");
      }

      // std::vector<std::string> errs = relay->GetErrorCodes();
      // for(int i = 0; i < errs.size(); i++){
      //    std::cout << "Relay " << i << ": " << relay->GetState(i) << '\t' << errs[i] << std::endl;
      // }

      //char buf[256];
      //sprintf(buf, "buffered %d (max %d), dropped %d, unknown %d, max flushed %d", gUdpPacketBufSize, fMaxBuffered, fCountDroppedPackets, fCountUnknownPackets, fMaxFlushed);
      //fEq->SetStatus(buf, "#00FF00");
      //fEq->WriteStatistics();
   }

   const std::string StatString(){
      char buf[64];
      sprintf(buf, "Relays: %d / %d / %d / %d", int(relaySet[0]), int(relaySet[1]), int(relaySet[2]), int(relaySet[3]));
      return std::string(buf);
   }

private:
   std::vector<bool> GetRelayStates(){
      std::vector<int> states = relay->GetStates();
      return std::vector<bool>(states.begin(), states.end());
   }

   bool SwitchRelay(int i, bool val){
      fMfe->Msg(MINFO, "SwitchRelay", "Switch channel %d %s", i, val?"ON":"OFF");
      bool success = (relay->SetState(i, val) == EPHIDGET_OK);
      return success;
   }

   bool busy = false;
};

/** \brief global wrapper for Midas callback of class function
 *
 */
void callback(INT hDB, INT hkey, INT index, void *feptr)
{
   Myfe* fe = (Myfe*)feptr;
   fe->fecallback(hDB, hkey, index);
}

static void usage()
{
   fprintf(stderr, "Usage: feRelay <name> ...\n");
   exit(1);
}

int main(int argc, char* argv[])
{
   // setbuf(stdout, NULL);
   // setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   std::string name = "";

   if (argc == 2) {
      name = argv[1];
   } else {
      usage(); // DOES NOT RETURN
   }

   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect("feRelay", __FILE__);
   if (err.error) {
      printf("Cannot connect, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *common = new TMFeCommon();
   common->EventID = 1;
   common->LogHistory = 1;
   //common->Buffer = "SYSTEM";

   TMFeEquipment* eq = new TMFeEquipment(mfe, "feRelay", common);
   eq->Init();
   eq->SetStatus("Starting...", "white");
   eq->ZeroStatistics();
   eq->WriteStatistics();

   mfe->RegisterEquipment(eq);

   Myfe* myfe = new Myfe(mfe, eq);

   mfe->RegisterRpcHandler(myfe);

   //mfe->SetTransitionSequenceStart(910);
   //mfe->SetTransitionSequenceStop(90);
   //mfe->DeregisterTransitionPause();
   //mfe->DeregisterTransitionResume();

   myfe->Init();

   mfe->RegisterPeriodicHandler(eq, myfe);

   eq->SetStatus(myfe->StatString().c_str(), "lightgreen");

   while (!mfe->fShutdownRequested) {
      mfe->PollMidas(10);
   }

   mfe->Disconnect();

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
